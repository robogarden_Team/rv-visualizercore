// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var ngAnnotate = require('gulp-ng-annotate');
var uglify = require('gulp-uglify');

// Lint Task
gulp.task('lint', function() {
	return gulp.src([
			'src/js/*.js',
		])
		.pipe(jshint({ devel: true }))
		.pipe(jshint.reporter('default'));
});

// Concatenate & Minify THREE JS
gulp.task('dist', function() {
	return gulp.src([
			"src/js/three.js",
			"src/js/Detector.js",
			"src/js/Projector.js",
			"src/js/CanvasRenderer.js",
			"src/js/Tween.js",
			"src/js/SPE.js",
			"src/js/postprocessing/EffectComposer.js",
			"src/js/postprocessing/RenderPass.js",
			"src/js/postprocessing/MaskPass.js",
			"src/js/postprocessing/ShaderPass.js",
			"src/js/shaders/CopyShader.js",
			"src/js/shaders/FXAAShader.js",
			"src/js/shaders/ConvolutionShader.js",
			"src/js/shaders/LuminosityHighPassShader.js",
			"src/js/postprocessing/UnrealBloomPass.js",
			"src/js/ErrorHandler.js",
			"src/js/CommonFunctions.js",
			"src/js/GameFlags.js",
			"src/js/visualizer/visualizer.js",
			"src/js/parser/regex.js",
			"src/js/VisualizerConfigurations.js",
			"src/js/visualizer/particles.js",
			"src/js/GameManager.js",
			"src/js/visualizer/scenemanager.js",
			"src/js/parser/parser.js",
			"src/js/parser/executer.js",
			"src/js/parser/commands.js",
			"src/js/visualizer/field.js",
			"src/js/visualizer/robot.js",
			"src/js/visualizer/objects.js",
			"src/js/loadingscene.js",
			"src/js/tester.js",
			"src/js/RoboVisualizer.js",
			"src/js/visualizer/sounds.js",
			"src/js/GameEvents.js",
			"src/js/visualizer/lcd.js",
		])
		.pipe(concat('rv-visualizer-core.js'))
		.pipe(gulp.dest('dist'))
		.pipe(rename('rv-visualizer-core.min.js'))
		.pipe(ngAnnotate())
		.pipe(uglify())
		.pipe(gulp.dest('dist'));
});

// Watch Files For Changes
gulp.task('watch', function() {
	gulp.watch([
		'src/js/*.js',
	], [
		'lint',
		'dist',
	]);
});

// Default Task
gulp.task('default', [
	'lint',
	'dist',
	'watch'
]);