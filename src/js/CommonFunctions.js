function loadJSON(filename, callback) {   
    var result = {
        status:"",
        body: ""
    };
    var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
    xobj.open('GET', filename, true);
    xobj.onreadystatechange = function load_json_file () {
        if(xobj.readyState == 4){
            if (xobj.status == "200") {
                result.status = "success";
                result.body = xobj.responseText;
                callback(result);
            }
            else
            {
                result.status = "failed";
                result.body = filename + " is not found, and readyState= " + xobj.readyState;
                callback(result);
            }
        }
          
    };
    xobj.send(null);  
 }

function Destroy(node)
{
    var i;
    if(node.children != undefined)
    {
        for ( i = node.children.length - 1; i >= 0 ; i -- ) 
        {
            Destroy(node.children[i]);
        }
    }
    if(node.parent != null)
        node.parent.remove(node);
    if(node.geometry!=undefined)
        node.geometry.dispose();
    if (node.material!=undefined)
    {
        if(node.material.dispose!=undefined)
            node.material.dispose();
        else
        {
            if(node.material.materials != undefined)
            {
                for(var a = node.material.materials.length-1; a >=0 ; a--)
                {
                    if(node.material.materials[a].map != null)
                        node.material.materials[a].map.dispose();
                    node.material.materials[a].dispose();
                }		
            }
        }
    }
}

function clear_objects() {
    pre_objects.forEach(function(v,i) {
        if(v.v_object.parent != undefined && v.v_object.parent != null)
            v.v_object.parent.remove(v);
        if(v.v_object.material != undefined)
        {
            if(v.v_object.material.dispose!=undefined)
                v.v_object.material.dispose();
            else
            {
                if(v.v_object.material.materials != undefined)
                {
                    for(var a = v.v_object.material.materials.length-1; a >=0 ; a--)
                    {
                        if(v.v_object.material.materials[a].map != null)
                            v.v_object.material.materials[a].map.dispose();
                        v.v_object.material.materials[a].dispose();
                    }
                }
            }
        }
        if(v.v_object.geometry != undefined)
            v.v_object.geometry.dispose();
        v = undefined;

    });

    pre_objects = [];

    all_objects.forEach(function(v,i) {
        if(v.parent != undefined && v.parent != null)
            v.parent.remove(v);
        if(v.material != undefined)
        {
            if(v.material.dispose!=undefined)
                v.material.dispose();
            else
            {
                if(v.material.materials != undefined)
                    if(v.material.materials != undefined)
                    {
                        for(var a = v.material.materials.length-1; a >=0 ; a--)
                        {
                            if(v.material.materials[a].map != null)
                                v.material.materials[a].map.dispose();
                            v.material.materials[a].dispose();
                        }
                    }
            }
        }
        if(v.geometry != undefined)
            v.geometry.dispose();
        v = undefined;

    });

    all_objects = [];
}