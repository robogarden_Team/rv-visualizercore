var initialEnvScript;
var xmlInput;
var sysCallback;

var visualizerCallback = function ()
{
    if(RoboVisualizer.gameFlags.isRestarting)
    {
        RoboVisualizer.gameFlags.isRestarting = false;
        if(xmlInput != undefined && xmlInput != null)
            RoboVisualizer.CompileTestSuit(xmlInput);
        else
        {
            RoboVisualizer.gameFlags.isRunning = false;
        }
    }
    else
    {
        // if(RoboVisualizer.gameFlags.windowFocused)
        // {
        var initFinish = new CustomEvent("onGridInitFinished",{'detail': {
                error: undefined
            }});
        document.body.dispatchEvent(initFinish);
        // }
        // else
        // {
        //     RoboVisualizer.gameFlags.shouldFinsishGridInit = true;
        // }
    }
};

var RoboVisualizer = {
    AssetsPath: "./assets/rg-assets/visualizer-assets/",
    TexturesPath:"./assets/rg-assets/visualizer-assets/assets/textures/",
    gameFlags: null,
    SceneInit: function(P_AssetsPath, windowFocus, callback){
        if(P_AssetsPath !== "" && P_AssetsPath !== null && P_AssetsPath != undefined)
        {
            RoboVisualizer.AssetsPath = P_AssetsPath;
            RoboVisualizer.TexturesPath = RoboVisualizer.AssetsPath + 'assets/textures/';
        }
            
        SceneManager.Initialize(windowFocus, callback);
    },
    LoadingSceneInit: function(callback)
    {
        loadingScreen.Initialize(callback);
    },
    SceneLoad: function(divID, sceneName, callback)
    {
        SceneManager.LoadScene(divID, sceneName, callback);
    },
    Init: function(_envScript, _callback)
    {
        if(RoboVisualizer.gameFlags.fieldInitialized)
            RoboVisualizer.ResetEnv();
        RoboVisualizer.gameFlags.fieldInitialized = true;
        if(!RoboVisualizer.gameFlags.isRunning)
        {
            RoboVisualizer.gameFlags.isRunning = true;
            GameManager.render();
        }
        sysCallback = _callback;
        initialEnvScript = _envScript;
        RoboVisualizer.gameFlags.showLoadingScene = false;
        visualizer.runMode == "env";
        Parser.ParseScript(initialEnvScript,visualizerCallback);
    },
    Pause: function()
    {
        RoboVisualizer.gameFlags.isRunning = false;
    },
    Continue: function()
    {
        if(RoboVisualizer.gameFlags.isStopped)
        {
            if(xmlInput != null && xmlInput != undefined)
            {
                var RunMission = new CustomEvent("onPlayBtnClicked", {'detail': {
                    input: xmlInput,
                    callback: undefined
                }});
                document.body.dispatchEvent(RunMission);
            }
        }
        else
        {
            RoboVisualizer.gameFlags.isRunning = true;
            GameManager.render();
        }
    },
    Stop: function()
    {
        if(RoboVisualizer.gameFlags.isStopped == false)
        {
            RoboVisualizer.gameFlags.isStopped = true;
            Executer.Abort(false);
            VisualizerTester.Abort();
            TWEEN.removeAll();
            RoboVisualizer.ResetEnv();
            RoboVisualizer.Init(initialEnvScript);
        }
    },
    Restart: function()
    {
        RoboVisualizer.gameFlags.isRestarting = true;
        Executer.Abort(false);
        VisualizerTester.Abort();
        TWEEN.removeAll();
        RoboVisualizer.ResetEnv();
        RoboVisualizer.Init(initialEnvScript);
    },
    CompileTestSuit: function(xmlDoc, callback)
    {
        xmlInput = xmlDoc;
        sysCallback = callback;
        errorsQueue = [];
        var parser = new DOMParser();
        var xml = parser.parseFromString(xmlDoc,"text/xml");
        var testSuite = xml.getElementsByTagName("Testsuit")[0];
        for(i = 0; i < testSuite.childNodes.length; i++)
        {
            var envScript = "";
            var motionScript = "";
            var testCaseNode = testSuite.childNodes[i].childNodes;
            envScript = testCaseNode[0].innerHTML;
            motionScript = testCaseNode[1].innerHTML;
            var testCase = {env: envScript, motion: motionScript};
            VisualizerTester.push(testCase);
        }
        VisualizerTester.ExecuteNextScript();
    },
    ResetEnv: function()
    {
        Field.clear_env();
    },
    ClearScene: function()
    {
        if(scene != undefined)
            Destroy(scene);
        if(sceneHUD != undefined)
            Destroy(sceneHUD);
    },
    WebglSupported: function()
    {
        return Detector.webgl;
    }
};