function GameFlags()
{
    this.playBtnClicked = true;
    this.pauseBtnClicked = true;
    this.continueBtnClicked = true;
    this.stopBtnClicked = true;
    this.isRunning = false;
    this.isStopped = false;
    this.isDisplaying = false;
    this.showLoadingScene = false;
    this.fieldInitialized = false;
    this.isRestarting = false;
    //this.shouldFinsishGridInit = false;
    this.particlesOn = true;
    this.inMissionPage = true;
    return this;
}