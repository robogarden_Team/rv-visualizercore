var Configurations = {
    RendererSize_x: 1366,
    RendererSize_y: 585,
    AspectRatio: 21/9,
    FOV: 45,
    cameraPos_x: 5,
    cameraPos_y: 9,
    cameraPos_z: 8.5,
    cameraLookAt_x:5,
    cameraLookAt_y:-1.1,
    cameraLookAt_z:-14,
    lcdSize_x: 600,
    lcdSize_y: 200,
    lcdPos_y: 60,
	orthoCamZoom: 1
};