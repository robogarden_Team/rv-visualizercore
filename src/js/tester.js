var scriptQueue = [];
var errorsQueue = [];

var envCallback = function()
{
    RoboVisualizer.gameFlags.fieldInitialized = true;
    var tobeExecuted = scriptQueue.shift();
    var motionscript = tobeExecuted.motion;
    setTimeout(function(){VisualizerTester.RunMotion(motionscript);}, 1000);
}

var testerCallback = function()
{
    if(scriptQueue.length > 0){
        var testFinishedEvent = new CustomEvent("testFinished");
        document.body.dispatchEvent(testFinishedEvent);
        setTimeout(function(){VisualizerTester.ExecuteNextScript();}, 2000);
    }

    else
    {
        RoboVisualizer.gameFlags.pauseBtnClicked = true;
        RoboVisualizer.gameFlags.playBtnClicked = false;
        RoboVisualizer.gameFlags.continueBtnClicked = true;
        RoboVisualizer.gameFlags.stopBtnClicked = true;
        setTimeout(function() {
            RoboVisualizer.gameFlags.isRunning = false;
            if(sysCallback != null && sysCallback != undefined)
                sysCallback(errorsQueue);   
        }, 2000/fps);
    }
}

var VisualizerTester = {
    push: function(script)
    {
        scriptQueue.push(script);
    },
    ExecuteNextScript: function()
    {
        if(RoboVisualizer.gameFlags.fieldInitialized)
            Field.clear_env();
        var envscript = scriptQueue[0].env;
        VisualizerTester.RunEnv(envscript);
    },
    RunEnv: function(script)
    {
        visualizer.runMode = "env";
        Parser.ParseScript(script, envCallback);
    },
    RunMotion: function(script)
    {
        visualizer.runMode = "motion";
        Parser.ParseScript(script, testerCallback);
    },
    Abort: function()
    {
        scriptQueue = [];
    }
}