//*************************************
//
//RoboGarden Game Engine
//
//*************************************


var rob_name;
var robot; //Used for movement
var robotMesh;
var rob_initPos; //Used to reset robot to its initial position
var targetPosition = {x: 0,z: 0};
var currentPosition = {x:0, z:0};
var currentRotation = {x:0, y:0};
var targetRotation = {x:0, y:0};
var initialRot;
var currentDistance= 0;

var Robots = {
    create_robot: function (x, y, name, type) {
        var assetPath = RoboVisualizer.AssetsPath + 'assets/models/' + type;
        var stored = resources.find(function(obj){return obj.key == assetPath});
        if(stored != undefined)
        {
            var object = new THREE.Mesh(stored.value.geometry, stored.value.material);
            object.scale.x = object.scale.y = object.scale.z = 1;
            object.name = name;
            rob_name = object.name;
            robotMesh = object;
            robot = new THREE.Object3D();
            THREE.SceneUtils.attach(robotMesh,DontDestroy.mainScene,robot);
            robot.position.z = -y;
            robot.position.x = x;
            var box = new THREE.Box3().setFromObject(robot);
            robot.position.y = (box.max.y-box.min.y) / 2;
            robotMesh.rotation.y = 90*Math.PI/180;
            rob_initPos = new THREE.Vector3(robot.position.x, robot.position.y, robot.position.z);
            initialRot = robotMesh.rotation.y;
            DontDestroy.mainScene.add(robot);
            commandCallback();
        }
        else
        {
            loadJSON(assetPath + ".js", function(response){
                if(response.status == "success")
                {
                    var jsonObj = JSON.parse(response.body);
                    if(DontDestroy.JSONLoader == null)
                        DontDestroy.JSONLoader = new THREE.JSONLoader();
                    var model = DontDestroy.JSONLoader.parse(jsonObj, RoboVisualizer.TexturesPath, function(){
                        if(model.geometry != undefined && model.materials != null)
                        {
                            var material = new THREE.MultiMaterial(model.materials);
                            var item = {};
                            item.key = assetPath;
                            item.value = {geometry: model.geometry, material: material};
                            resources.push(item);
                            var object = new THREE.Mesh(model.geometry, material);
                            object.scale.x = object.scale.y = object.scale.z = 1;
                            object.name = name;
                            rob_name = object.name;
                            robotMesh = object;
                            robot = new THREE.Object3D();
                            THREE.SceneUtils.attach(robotMesh,DontDestroy.mainScene,robot);
                            robot.position.z = -y;
                            robot.position.x = x;
                            var box = new THREE.Box3().setFromObject(robot);
                            robot.position.y = (box.max.y-box.min.y)/2;
                            robotMesh.rotation.y = 90*Math.PI/180;
                            rob_initPos = new THREE.Vector3(robot.position.x, robot.position.y, robot.position.z);
                            initialRot = robotMesh.rotation.y;
                            DontDestroy.mainScene.add(robot);
                            commandCallback();
                        }
                        else
                        {
                            var errorType = "LoadingError";
                            var errorMsg = "Error loading \"" + assetPath + "\"";
                            var caller = arguments.callee.name;
                            var err = new VisualizerError(errorType, errorMsg, caller);
                            commandCallback(err);
                        }
                    });
                }
                else
                {
                    var errorType = "LoadingError";
                    var errorMsg = "Error loading \"" + assetPath + "\"";
                    var caller = arguments.callee.name;
                    var err = new VisualizerError(errorType, errorMsg, caller);
                    commandCallback(err);
                }
            });
        }
    },

    move_robot: function (name, direction, distance, speed) 
    {
        if(distance < 0)
        {
            if(direction == "forward")
                direction = "backward";
            else
                direction = "forward";
            distance *= -1;
        }
        if (direction == 'forward') 
        {

            currentPosition.x = robot.position.x;
            currentPosition.z = robot.position.z;  
            targetPosition.x = currentPosition.x + distance*Math.cos(robotMesh.rotation.y);
            targetPosition.z = currentPosition.z - distance*Math.sin(robotMesh.rotation.y);
            var movementTween = new TWEEN.Tween(currentPosition)
            .to(targetPosition, distance*1000/(speed*visualizer.speedFactor))
            .delay(0)
            .easing(TWEEN.Easing.Linear.None)
            .onUpdate(function(){
                robot.position.x = currentPosition.x;
                robot.position.z = currentPosition.z;
            })
            .onComplete(function(){
                commandCallback();
            })
            .start();

        }

        else if (direction == 'backward') 
        {

            currentPosition.x = robot.position.x;
            currentPosition.z = robot.position.z;
            targetPosition.x = currentPosition.x - distance*Math.cos(robotMesh.rotation.y);
            targetPosition.z = currentPosition.z + distance*Math.sin(robotMesh.rotation.y);
            var movementTween = new TWEEN.Tween(currentPosition)
            .to(targetPosition, distance*1000/(speed*visualizer.speedFactor))
            .delay(0)
            .easing(TWEEN.Easing.Linear.None)
            .onUpdate(function(){
                robot.position.x = currentPosition.x;
                robot.position.z = currentPosition.z;
            })
            .onComplete(function(){
                commandCallback();
            })
            .start();

        }
    },

    rotate_robot: function (name, angle, radius, speed) {
        var radAngle = angle*Math.PI/180;
        currentRotation.y = robotMesh.rotation.y;
        targetRotation.y = currentRotation.y + radAngle;
        if(radius == 0)
        {
            var rotationTween = new TWEEN.Tween(currentRotation)
            .to(targetRotation, 1000/(speed*visualizer.speedFactor))
            .delay(0)
            .easing(TWEEN.Easing.Linear.None)
            .onUpdate(function(){
                robotMesh.rotation.y = currentRotation.y;
            })
            .onComplete(function(){
                commandCallback();
            })
            .start();

        }
        else
        {

            var center = {x: 0, z: 0};
            if(angle < 0)
            {
                center.x = robot.position.x+radius*Math.sin(robotMesh.rotation.y);
                center.z = robot.position.z+radius*Math.cos(robotMesh.rotation.y);
            }
            else
            {
                center.x = robot.position.x-radius*Math.sin(robotMesh.rotation.y);
                center.z = robot.position.z-radius*Math.cos(robotMesh.rotation.y);
            }
            var distance = ((Math.abs(radAngle)%(Math.PI*2))/(Math.PI*2))*2*Math.PI*radius;
            if(distance == 0)
                distance = 2*Math.PI*radius;
            var rotationTween = new TWEEN.Tween(currentRotation)
            .to(targetRotation,distance*1000/(speed*visualizer.speedFactor))
            .delay(0)
            .easing(TWEEN.Easing.Linear.None)
            .onUpdate(function(){
                robotMesh.rotation.y = currentRotation.y;
                if(angle < 0)
                {
                    robot.position.x = center.x - radius*Math.cos(initialRot - currentRotation.y);
                    robot.position.z = center.z - radius*Math.sin(initialRot - currentRotation.y);
                }
                else
                {
                    robot.position.x = center.x - radius*Math.cos(initialRot + currentRotation.y);
                    robot.position.z = center.z + radius*Math.sin(initialRot + currentRotation.y);
                }
            })
            .onComplete(function(){
                commandCallback();
            })
            .start();

        }
    }
}
