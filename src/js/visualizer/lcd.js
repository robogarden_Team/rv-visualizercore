function LCD()
{
    lcdClock = new THREE.Clock();
    if(DontDestroy.lcdCamera == undefined)
        DontDestroy.lcdCamera = new THREE.OrthographicCamera(-Configurations.lcdSize_x/2, Configurations.lcdSize_x/2, Configurations.lcdSize_y/2, -Configurations.lcdSize_y/2, 0, 30 );
    if(DontDestroy.lcdScene == undefined)
        DontDestroy.lcdScene = new THREE.Scene();
    this.width = Configurations.lcdSize_x;
    this.height = Configurations.lcdSize_y;
    this.hudCanvas = document.createElement('canvas');
    this.hudCanvas.width = this.width;
    this.hudCanvas.height = this.height;
    this.hudBitmap = this.hudCanvas.getContext('2d');
    this.hudBitmap.font = "Normal 20px Arial";
    this.hudBitmap.textAlign = 'left';
    this.hudBitmap.fillStyle = "rgba(0,0,0,0.5)";
    this.hudBitmap.fillRect(0, 0, this.width, this.hudCanvas.height);
    this.hudBitmap.fillStyle = "rgba(255,255,255,1)";
    this.hudTexture = new THREE.Texture(this.hudCanvas);
    this.hudTexture.needsUpdate = true;
    this.lineCounter = 1;
    // Create HUD material.
    var material = new THREE.MeshBasicMaterial( {map: this.hudTexture, transparent:true} );

    // Create plane to render the HUD. This plane fill the whole screen.
    var planeGeometry = new THREE.PlaneGeometry( this.width, this.height );
    var plane = new THREE.Mesh( planeGeometry, material );
    plane.position.y = Configurations.lcdPos_y;
    plane.scale.x = 0.7;
    plane.scale.y = 0.4;
    this.gameObject = plane;
    DontDestroy.lcdScene.add(this.gameObject);
    RoboVisualizer.gameFlags.isDisplaying = false;
}