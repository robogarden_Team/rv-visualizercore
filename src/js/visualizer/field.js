//*************************************
//
//RoboGarden Game Engine
//
//*************************************


var grid;
var gridSize = {width: 0, height: 0};
var gridObjects;
var printRenderer;
var cellsTxt;
var screenTxt;

var Field = {
    field_init: function (Width, Height, color, textureName) {
        gridSize.width = Width;
        gridSize.height = Height;
        grid = new Array(Height);
        gridObjects = new Array(Height);
        cellsTxt = new Array(Height);
        var geometry = new THREE.BoxGeometry(1, 0.02, 1);
        var assetPath = RoboVisualizer.AssetsPath + "assets/textures/" + textureName;
        var stored = resources.find(function(obj){return obj.key == assetPath});
        if(stored != undefined)
        {
            var material = new THREE.MeshLambertMaterial({map: stored.value, color: color, transparent:true});
            Field.BuildGrid(geometry, material, Width, Height);
        }
        else
        {
            var loader = new THREE.TextureLoader();
            loader.load(assetPath + ".png",
                function(texture){
                    var item = {};
                    item.key = assetPath;
                    item.value = texture;
                    resources.push(item);
                    var material = new THREE.MeshLambertMaterial({map: texture, color: color, transparent:true});
                    Field.BuildGrid(geometry, material, Width, Height);
                },
                function()
                {
                    // on progress
                },
                function Field_Initialize()
                {
                    var errorType = "LoadingError";
                    var errorMsg = "Error loading \"" + assetPath + "\"";
                    var caller = arguments.callee.name;
                    var err = new VisualizerError(errorType, errorMsg, caller);
                    commandCallback(err);
                }
            );
        }
    },

    BuildGrid: function(geo, mat, Width, Height)
    {
        var cellMesh;
        var cell_Mesh_default = new THREE.Mesh(geo, mat);
        for(var i = 1; i <= Height; i++)
        {
            if(grid == undefined)           grid = new Array(Height);
            if(gridObjects == undefined)    gridObjects = new Array(Height);
            if(cellsTxt == undefined)       cellsTxt = new Array(Height);

            grid[i] = new Array(Width);
            gridObjects[i] = new Array(Width);
            cellsTxt[i] = new Array(Width);
            for(var j = 1; j <= Width; j++)
            {
                cellMesh = cell_Mesh_default.clone();
                cellMesh.position.x = j;
                cellMesh.position.y = 0;
                cellMesh.position.z = -i;
               
                grid[i][j] = cellMesh;
                DontDestroy.mainScene.add(cellMesh);
                
                gridObjects[i][j] = 0;
            }
        }
        commandCallback();
    },
    
//change cell color, most likely will be changed when grid is updated.
    setCellColor: function(x, y, material, callback)
    {
        if(grid != undefined && grid[y] != undefined)
        {
            if(grid[y][x] != undefined && grid[y][x] != null)
            {
                if(grid[y][x].material != undefined)
                {
                    grid[y][x].material.map.dispose();
                    grid[y][x].material.dispose();
                    grid[y][x].material.needsUpdate = true;
                    grid[y][x].material = material;
                }
            }
        }
        
        callback();
    },
    createMaterial: function(textureName, color, callback)
    {
        var assetPath = RoboVisualizer.AssetsPath + "assets/textures/" + textureName;
        var stored = resources.find(function(obj){return obj.key == assetPath});
        if(stored != undefined)
        {
            callback(new THREE.MeshLambertMaterial({map: stored.value, color: color, transparent:true}));
        }
        else
        {
            var material;
            if(DontDestroy.TextureLoader == undefined)
                DontDestroy.TextureLoader = new THREE.TextureLoader();
            var full_path = assetPath + ".jpg";
            if(textureName == "transparent")
                full_path = assetPath + ".png";
            DontDestroy.TextureLoader.load(full_path,
                function(texture)
                {
                    texture.anisotropy = DontDestroy.renderer.getMaxAnisotropy();
                    var item = {};
                    item.key = assetPath;
                    item.value = texture;
                    resources.push(item);
                    callback(new THREE.MeshLambertMaterial({map: texture, color: color, transparent:true}));
                },
                function()
                {
                    // on progress
                },
                function Field_ChangeColor()
                {
                    var errorType = "LoadingError";
                    var errorMsg = "Error loading \"" + assetPath + "\"";
                    var caller = arguments.callee.name;
                    var err = new VisualizerError(errorType, errorMsg, caller);
                    commandCallback(err);
                }
            );
        }
    },
    change_color: function (x, y, color, textureName, callback) {
            Field.createMaterial(textureName, color, function(material){
                Field.setCellColor(x, y, material, callback);
            });
    },
    set_row_color: function(color, texture, row, pattern)
    {
        var callback;
        Field.createMaterial(texture, color, function(material){
            var indx = 0;
            callback = function()
            {
                indx++;
                if(indx > pattern.length)
                    commandCallback();
                else
                {
                    while(pattern[indx] != 'x')
                    {
                        if(indx > pattern.length)
                        {
                            commandCallback();
                            return;
                        }
                        else
                            indx++;
                    }
                        
                    Field.setCellColor(indx, row, material, callback);
                }
            }
            while(pattern[indx] != 'x')
            {
                if(indx > pattern.length)
                {
                    commandCallback();
                    return;
                }
                else
                    indx++;
            }
            Field.setCellColor(indx, row, material, callback);
        });
    },
    set_col_color: function(color, texture, col, pattern)
    {
        var callback;
        Field.createMaterial(texture, color, function(material){
            var indx = 0;
            callback = function()
            {
                indx++;
                if(indx > pattern.length)
                    commandCallback();
                else
                {
                    if(pattern[indx] == 'x')
                        Field.setCellColor(col, indx, material, callback);
                    else
                        callback();
                }
                    
            }
            while(pattern[indx] != 'x')
            {
                if(indx > pattern.length)
                {
                    commandCallback();
                    return;
                }
                else
                    indx++;
            }
            Field.setCellColor(col, indx, material, callback);
        });
    },
    clear_env: function (cb) 
    {
        if(grid != undefined)
        {
            for(i = 1; i <= gridSize.height; i++)
            {
                for(j = 1; j <= gridSize.width; j++)
                {
                    if(DontDestroy.mainScene != undefined)
                    {
                        if(grid[i] != undefined)
                        {
                            if(grid[i][j] != undefined)
                                Destroy(grid[i][j]);
                        }
                        if(gridObjects[i] != undefined)
                        {
                            if(gridObjects[i][j] != undefined)
                                Destroy(gridObjects[i][j]);
                        }
                        if(cellsTxt[i] != undefined)
                        {
                            if(cellsTxt[i][j] != undefined)
                                Destroy(cellsTxt[i][j]);
                        }
                    }
                }
            }
            grid = undefined;
            gridObjects = undefined;
            cellsTxt = undefined;
            if(robot != undefined)
                Destroy(robot);
            Field.clear_lcd();
            RoboVisualizer.gameFlags.fieldInitialized = false;
        }
        if(cb)
            commandCallback();
    },
    play_sound: function(type)
    {
        var assetPath = RoboVisualizer.AssetsPath + "assets/sfx/" + type;
        var stored = resources.find(function(obj){return obj.key == assetPath});
        if(stored != null)
        {
            var audio = stored.value;
            audio.play();
            commandCallback();
        }
        else
        {
            var audio = new Audio(assetPath + ".mp3");
            var item = {};
            item.key = assetPath;
            item.value = audio;
            resources.push(item);
            audio.play();
            commandCallback();
        }
    },
    print_msg: function (msg) {
            msg = msg.replace(/\"/g,'');
            lcd.hudBitmap.fillText(msg, 30, 50+ lcd.lineCounter*30);
            lcd.lineCounter++;

            if(RoboVisualizer.gameFlags.isDisplaying == false)
            {
                RoboVisualizer.gameFlags.isDisplaying = true;
                lcdClock.start();
                lcdTimeout=3;
            }
            else
                lcdTimeout+=3;
            lcd.hudTexture.needsUpdate = true;
            // var printLCDEvent = new CustomEvent("printLCDEvent", {
            //     'detail': {
            //     msg: msg
            //     }
            // });
            //document.body.dispatchEvent(printLCDEvent);
            if(visualizer.mode == "dream" && visualizer.runMode == "motion") {
                console.log("acting print lcd ");
                var actPrintLcd = new CustomEvent("actPrintLcd", {
                    'detail': {
                        msg: msg
                    }
                });
                document.body.dispatchEvent(actPrintLcd);
                setTimeout(function(){ commandCallback();}, visualizer.dreamSpeedSlowFactor);
            }
            else
                commandCallback();
    },
    clear_lcd: function(cb){
        if(lcd != undefined)
            lcd.lineCounter = 1;
        if(DontDestroy.lcdScene != undefined)
        {
            Destroy(DontDestroy.lcdScene);
            lcd = new LCD();
        }
        // var clearLCDEvent = new CustomEvent("clearLCDEvent");
        // document.body.dispatchEvent(clearLCDEvent);
        if(cb)
            commandCallback();
    },
    print_on_cell: function(x,y,msg, color, fontSize)
    {
        if(cellsTxt != undefined){
            if(cellsTxt[y] != undefined)
            {
                if(cellsTxt[y][x] != undefined && cellsTxt[y][x] != null)
                    DontDestroy.mainScene.remove(cellsTxt[y][x]);
            }
        }
        
        msg = msg.replace(/\"/g,'');
        if(!isNaN(Number(msg))){
            msg = parseFloat(msg).toString();
        }
        var assetPath = RoboVisualizer.AssetsPath + 'assets/fonts/helvetiker_regular.typeface';
        var stored = resources.find(function(obj){return obj.key == assetPath});
        if(stored != undefined)
        {
            var textGeo = new THREE.TextGeometry( msg, {
                font: stored.value,
                size: fontSize, // font size
                height: 0, // how much extrusion (how thick / deep are the letters)
                curveSegments: 5,
                bevelThickness: 0.1,
                bevelSize: 0.1,
                bevelEnabled: true
            });
            THREE.GeometryUtils.center(textGeo);
            var textMaterial = new THREE.MeshPhongMaterial( { color: color, specular: color } );
            var mesh = new THREE.Mesh( textGeo, textMaterial );
            mesh.scale.set(0.04, 0.04, 0.001);
            mesh.position.z = -y + 0.1;
            mesh.position.x = x;
            mesh.position.y = 0.06;
            mesh.rotation.x = -90*Math.PI/180;
            mesh.castShadow = true;
            mesh.receiveShadow = true;
            if(cellsTxt != undefined){
                if(cellsTxt[y] != undefined)
                cellsTxt[y][x] = mesh;
            }
            DontDestroy.mainScene.add( mesh );
            if(visualizer.mode == "dream" && visualizer.runMode == "motion") {
                console.log("acting print to cell " + msg + " "+color);
                var actPrintCell = new CustomEvent("actPrintCell", {
                    'detail': {
                        msg: msg,
                        color: color
                    }
                });
                document.body.dispatchEvent(actPrintCell);
                setTimeout(function(){ commandCallback();}, visualizer.dreamSpeedSlowFactor);
            }
            else
                commandCallback();
        }
        else
        {
            var loader = new THREE.FontLoader();
            loader.load( assetPath + '.json',
                function ( font )
                {
                    var item = {};
                    item.key = assetPath;
                    item.value = font;
                    resources.push(item);
                    var textGeo = new THREE.TextGeometry( msg, {
                        font: font,
                        size: fontSize, // font size
                        height: 0, // how much extrusion (how thick / deep are the letters)
                        curveSegments: 5,
                        bevelThickness: 0.1,
                        bevelSize: 0.1,
                        bevelEnabled: true
                    });
                    THREE.GeometryUtils.center(textGeo);
                    var textMaterial = new THREE.MeshPhongMaterial( { color: color, specular: color } );
                    var mesh = new THREE.Mesh( textGeo, textMaterial );
                    mesh.scale.set(0.04, 0.04, 0.001);
                    mesh.position.z = -y + 0.1;
                    mesh.position.x = x;
                    mesh.position.y = 0.06;
                    mesh.rotation.x = -90*Math.PI/180;
                    mesh.castShadow = true;
                    mesh.receiveShadow = true;
                    if(cellsTxt != undefined){
                        if(cellsTxt[y] != undefined)
                            cellsTxt[y][x] = mesh;
                    }
                    
                    DontDestroy.mainScene.add( mesh );
                    commandCallback();
                },
                function()
                {
                    // on progress
                },
                function Field_PrintCell()
                {
                    var errorType = "LoadingError";
                    var errorMsg = "Error loading \"" + assetPath + "\"";
                    var caller = arguments.callee.name;
                    var err = new VisualizerError(errorType, errorMsg, caller);
                    commandCallback(err);
                }
            );
        }
    }
}