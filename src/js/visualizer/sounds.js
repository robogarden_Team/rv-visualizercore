var Sounds = {
    SetupSound : function(callback)
    {
        if(DontDestroy.audioListener == undefined)
            DontDestroy.audioListener = new THREE.AudioListener();
        if(DontDestroy.audioLoader == undefined)
            DontDestroy.audioLoader = new THREE.AudioLoader();
        if(DontDestroy.sounds == undefined)
            DontDestroy.sounds = [];
        if(DontDestroy.sounds["win"] == undefined)
        {
            DontDestroy.sounds["win"] = new THREE.Audio( DontDestroy.audioListener );
            DontDestroy.audioLoader.load( RoboVisualizer.AssetsPath + 'assets/sfx/win.mp3', function( buffer ) {
                DontDestroy.sounds["win"].setBuffer( buffer );
                DontDestroy.sounds["win"].setLoop(false);
                DontDestroy.sounds["win"].setVolume(0.6);
            });
        }
        if(DontDestroy.sounds["lose"] == undefined)
        {
            DontDestroy.sounds["lose"] = new THREE.Audio( DontDestroy.audioListener );
            DontDestroy.audioLoader.load( RoboVisualizer.AssetsPath + 'assets/sfx/lose.mp3', function( buffer ) {
                DontDestroy.sounds["lose"].setBuffer( buffer );
                DontDestroy.sounds["lose"].setLoop(false);
                DontDestroy.sounds["lose"].setVolume(0.6);
            });
        }
        if(DontDestroy.sounds[SceneManager.SceneName] == undefined)
        {
            DontDestroy.sounds[SceneManager.SceneName] = new THREE.Audio( DontDestroy.audioListener );
            DontDestroy.audioLoader.load( RoboVisualizer.AssetsPath + 'assets/sfx/' + SceneManager.SceneName + '.mp3', function( buffer ) {
                DontDestroy.sounds[SceneManager.SceneName].setBuffer( buffer );
                DontDestroy.sounds[SceneManager.SceneName].setLoop(true);
                DontDestroy.sounds[SceneManager.SceneName].setVolume(0.4);
                callback();
            });
        }
        else
        {
            callback();
        }
    },
    PlayMainTheme: function()
    {
        if(DontDestroy.sounds[SceneManager.SceneName] != undefined)
            DontDestroy.sounds[SceneManager.SceneName].play();
    },
    StopMainTheme: function()
    {
        if(DontDestroy.sounds != undefined)
        {
            if(DontDestroy.sounds[SceneManager.SceneName] != undefined)
            {
                if(DontDestroy.sounds[SceneManager.SceneName].isPlaying)
                    DontDestroy.sounds[SceneManager.SceneName].stop();
            }
        }
    },
    PlayWinSound : function()
    {
        if(DontDestroy.sounds["win"] != undefined)
            DontDestroy.sounds["win"].play();
    },
    PlayLoseSound : function()
    {
        if(DontDestroy.sounds["lose"] != undefined)
            DontDestroy.sounds["lose"].play();
    },
    Mute : function()
    {
        //DontDestroy.audioListener.setMasterVolume(1-DontDestroy.audioListener.getMasterVolume());
        DontDestroy.audioListener.setMasterVolume(0);
    },
    Play : function()
    {
        DontDestroy.audioListener.setMasterVolume(1);
    },
}