var container;
var lastFrameUpdate = new Date;
var fps = 60;
var terrain;
var skybox;
var lcd;
var skyboxSize = {x: 0, y: 0, z: 0};
//bloom effect
var effectFXAA, bloomPass, renderScene;
var hdrCubeMap;
var composer;
var standardMaterial;
var hdrCubeRenderTarget;



function createMaterial( path ) {
		var texture = THREE.ImageUtils.loadTexture(path);
		var material = new THREE.MeshBasicMaterial( { map: texture, overdraw: 0.5, fog: false  } );
		return material; 
}

function CreateSkybox(textures)
{
    var materials = [
        createMaterial(RoboVisualizer.AssetsPath + 'assets/Skybox/' + textures[0] ), // right
        createMaterial(RoboVisualizer.AssetsPath + 'assets/Skybox/' + textures[1] ), // left
        createMaterial(RoboVisualizer.AssetsPath + 'assets/Skybox/' + textures[2] ), // top
        createMaterial(RoboVisualizer.AssetsPath + 'assets/Skybox/' + textures[3] ), // bottom
        createMaterial(RoboVisualizer.AssetsPath + 'assets/Skybox/' + textures[4] ), // back
        createMaterial(RoboVisualizer.AssetsPath + 'assets/Skybox/' + textures[5] )  // front
        ];         
    // Create a large cube
    skybox = new THREE.Mesh( new THREE.BoxGeometry(skyboxSize.x, skyboxSize.y, skyboxSize.z), new THREE.MeshFaceMaterial( materials ) );           
    // Set the x scale to be -1, this will turn the cube inside out
    skybox.scale.set(-1,1,1);
    DontDestroy.mainScene.add( skybox );  
}

function SetupRenderer()
{
    if(DontDestroy.renderer == undefined)
        DontDestroy.renderer = new THREE.WebGLRenderer({ alpha: true });
    DontDestroy.renderer.setPixelRatio( window.devicePixelRatio );
    DontDestroy.renderer.setSize(Configurations.RendererSize_x, Configurations.RendererSize_y);
    DontDestroy.renderer.autoClear = false;
    DontDestroy.renderer.shadowMapEnabled = true;
    DontDestroy.renderer.shadowMapSoft = true;
}

var Scenes = {};

Scenes["winter"] = {
    Initialize: function(divID, callback)
    {
        // Initialization
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        DontDestroy.mainScene.fog = new THREE.Fog(0xe6edec, 20, 70);
        // create skybox
        skyboxSize.x = 120;
        skyboxSize.y = 60;
        skyboxSize.z = 120;
        CreateSkybox(["blizzard_RT.jpg", "blizzard_LF.jpg", "blizzard_UP.jpg", "blizzard_DN.jpg", "blizzard_BK.jpg", "blizzard_FT.jpg"]);
        // setup LCD
        lcd = new LCD();
        // setup lights
        
        var ambientLight = new THREE.AmbientLight(0xFFFFFF, 1.2); // soft white light
        DontDestroy.mainScene.add(ambientLight);
       
        var spotLight = new THREE.SpotLight(0xFFFFFF, 1, 250, 0.21, 0, 0);
        spotLight.position.set( 90, 90, -100 );
        spotLight.castShadow = true;
        spotLight.shadowCameraVisible = true;
        spotLight.shadowDarkness = 0.5;
        //Set up shadow properties for the light
        spotLight.shadow.mapSize.width = 1024;  // default
        spotLight.shadow.mapSize.height = 1024; // default
        spotLight.shadow.camera.near = 0.5;       // default
        spotLight.shadow.camera.far = 50      // default
        DontDestroy.mainScene.add( spotLight );
        
        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );

        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var materialCounter = 0;
        var assetPath1 = RoboVisualizer.AssetsPath + 'assets/models/winter_terrain';
        if(DestroyOnJourney.Terrain == undefined || DestroyOnJourney.Terrain == null)
        {
            loadJSON(assetPath1 + ".js", function(response){
                var jsonObj = JSON.parse(response.body);
                if(DontDestroy.JSONLoader == null)
                    DontDestroy.JSONLoader = new THREE.JSONLoader();
                var model = DontDestroy.JSONLoader.parse(jsonObj, RoboVisualizer.TexturesPath, function(){
                    if(model.geometry != undefined && model.materials != null)
                    {
                        var material = new THREE.MultiMaterial(model.materials);
                        DestroyOnJourney.Terrain = new THREE.Mesh(model.geometry, material);
                        DestroyOnJourney.Terrain.traverse ( function (child) {
                            if (child instanceof THREE.Mesh) {
                                child.castShadow = true;
                                child.receiveShadow = true;
                            }
                        });
                        DestroyOnJourney.Terrain.position.x = 5.5;
                        DestroyOnJourney.Terrain.position.y = 0;
                        DestroyOnJourney.Terrain.position.z = -5.5;
                        DestroyOnJourney.Terrain.scale.x = DestroyOnJourney.Terrain.scale.y = DestroyOnJourney.Terrain.scale.z = 1.02;
                        DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
                        materialCounter++;
                        if(materialCounter == model.materials.length)
                            callback();
                    }
                    else
                    {
                        var errorType = "LoadingError";
                        var errorMsg = "Error loading \"" + assetPath1 + "\"";
                        var caller = arguments.callee.name;
                        var err = new VisualizerError(errorType, errorMsg, caller);
                        visualizer.SetError(err);
                        callback(err);
                    }
                });
            });
        }
        else
        {
            DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
            callback();
        }
    },
    LateInitialize: function()
    {
        //bloom render pass

        renderScene = new THREE.RenderPass(scene, camera);
        // renderScene.clear = true;
        effectFXAA = new THREE.ShaderPass(THREE.FXAAShader);
        effectFXAA.uniforms['resolution'].value.set(1 / window.innerWidth, 1 / window.innerHeight);

        var copyShader = new THREE.ShaderPass(THREE.CopyShader);
        copyShader.renderToScreen = true;

        bloomPass = new THREE.UnrealBloomPass(new THREE.Vector2(window.innerWidth, window.innerHeight), 1.5, 0.4, 0.85);//1.0, 9, 0.5, 512);
        composer = new THREE.EffectComposer(DontDestroy.renderer);
        composer.setSize(window.innerWidth, window.innerHeight);
        composer.addPass(renderScene);
        composer.addPass(effectFXAA);
        composer.addPass(bloomPass);
        composer.addPass(copyShader);
        //renderer.toneMapping = THREE.ReinhardToneMapping;
         DontDestroy.renderer.gammaInput = true;
         DontDestroy.renderer.gammaOutput = true;
    }
};

Scenes["winter2d"] = {
    Initialize: function(divID, callback)
    {
        console.log('Start winter initialization');
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        if(DestroyOnJourney.BGScene == undefined)
            DestroyOnJourney.BGScene = new THREE.Scene();
        if(DontDestroy.BGCamera == undefined)
            DontDestroy.BGCamera = new THREE.OrthographicCamera(-Configurations.RendererSize_x/2, Configurations.RendererSize_x/2, Configurations.RendererSize_y/2, -Configurations.RendererSize_y/2, 0, 30);
        if(DontDestroy.TextureLoader == undefined)
            DontDestroy.TextureLoader = new THREE.TextureLoader();
        console.log('Start initialization LCD');
        // setup LCD
        lcd = new LCD();
        // setup lights
        console.log('finish initialization LCD');
        DestroyOnJourney.ambientLight = new THREE.AmbientLight(0xFFFFFF, 1.2); // soft white light
        DontDestroy.mainScene.add( DestroyOnJourney.ambientLight );
    
        // DestroyOnJourney.spotLight = new THREE.SpotLight(0xFFFFFF, 1, 250, 0.21, 0, 0);
        // DestroyOnJourney.spotLight.position.set( 90, 90, -100 );
        // DestroyOnJourney.spotLight.castShadow = true;
        // DestroyOnJourney.spotLight.shadowCameraVisible = true;
        // DestroyOnJourney.spotLight.shadowDarkness = 0.5;
        // //Set up shadow properties for the light
        // DestroyOnJourney.spotLight.shadow.mapSize.width = 1024;  // default
        // DestroyOnJourney.spotLight.shadow.mapSize.height = 1024; // default
        // DestroyOnJourney.spotLight.shadow.camera.near = 0.5;       // default
        // DestroyOnJourney.spotLight.shadow.camera.far = 50      // default
        // DontDestroy.mainScene.add( DestroyOnJourney.spotLight );
        
        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );
        console.log('Finish winter initialization before terrain');
        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        console.log('Start winter terrain initialization');
        var spriteMap = DontDestroy.TextureLoader.load(RoboVisualizer.TexturesPath + "winter2d.jpg",
            function(texture)
            {
                console.log('Start winter terrain initialization after loading');
                texture.needsUpdate = true;
                var spriteMaterial = new THREE.SpriteMaterial({map: texture});
                var sprite = new THREE.Sprite(spriteMaterial);
                sprite.scale.set(Configurations.RendererSize_x, Configurations.RendererSize_y, 0.1);
                sprite.renderOrder = 1;
                DestroyOnJourney.BGScene.add(sprite);
                RoboVisualizer.gameFlags.showLoadingScene = false;
                console.log('finish winter terrain initialization');
                callback();
            },
            function()
            {},
            function loadingScene_Initialize()
            {
                var errorType = "LoadingError";
                var errorMsg = "Error loading \"" + RoboVisualizer.TexturesPath + "winter2d.jpg" + "\"";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                visualizer.SetError(err);
                callback(err);
            }
        );
    },
    LateInitialize: function()
    {
        // if(DestroyOnJourney.particleSystem == undefined)
        // {
        //     if(DontDestroy.ParticlesCam == undefined)
        //         DontDestroy.ParticlesCam = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        //     if(DontDestroy.ParticlesScene == undefined)
        //         DontDestroy.ParticlesScene = new THREE.Scene();
        //     Particles.ParticlSystem.push(new SPE.Group({
        //         texture: {
        //             value: THREE.ImageUtils.loadTexture(RoboVisualizer.TexturesPath + 'snowflake.png')
        //         },
        //         blending: THREE.NormalBlending,
        //         fog: true
        //     }));
        //
        //     emitter = new SPE.Emitter({
        //         particleCount: 250,
        //         maxAge: {
        //             value: 3,
        //         },
        //         position: {
        //             value: new THREE.Vector3( 0, 15, -20 ),
        //             spread: new THREE.Vector3( 60, 15, 40 )
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, -3, 0 )
        //         },
        //         wiggle: {
        //             spread: 10
        //         },
        //         size: {
        //             value: 1,
        //             spread: 1
        //         },
        //         opacity: {
        //             value: [ 0.9, 0.9, 0.9 ]
        //         },
        //         color: {
        //             value: new THREE.Color( 1, 1, 1 ),
        //             spread: new THREE.Color( 0.1, 0.1, 0.1 )
        //         },
        //         angle: {
        //             value: [ 0, Math.PI * 0.125 ]
        //         }
        //     });
        //
        //     Particles.ParticlSystem[0].addEmitter( emitter );
        //     DontDestroy.ParticlesScene.add( Particles.ParticlSystem[0].mesh );
        //     Particles.clock = new THREE.Clock();
        // }
        // Particles.isReady = true;
    }
}

Scenes["birthday"] = {
    Initialize: function(divID, callback)
    {
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        DontDestroy.mainScene.fog = new THREE.Fog(0x3abbcd, 30, 200);
        // setup LCD
        lcd = new LCD();

        // lights
        var ambientLight = new THREE.AmbientLight(0xffffff, 1.8); // soft white light
        DontDestroy.mainScene.add( ambientLight );
       
        var spotLight = new THREE.SpotLight(0xffffff, 0.5, 250, 0.21, 0, 0);
        spotLight.position.set(60, 90, -100 );
        //Set up shadow properties for the light
        spotLight.castShadow = true;
        spotLight.shadowCameraVisible = true;
        spotLight.shadow.mapSize.width = 2048;  // default
        spotLight.shadow.mapSize.height = 2048; // default
        spotLight.shadow.camera.near = 0.5;       // default
        spotLight.shadow.camera.far = 50      // default
        spotLight.shadowDarkness = 1;
        spotLight.target.position.set(5, 3, 0);
        spotLight.bias = 0.001;
        DontDestroy.mainScene.add( spotLight );

        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );

        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var tex = new Array();
        var tex_sequence = 0;
        var materialCounter = 0;
        var assetPath = RoboVisualizer.AssetsPath + 'assets/models/birthday';
        if(DestroyOnJourney.Terrain == null || DestroyOnJourney.Terrain == undefined)
        {
            loadJSON(assetPath + ".js", function(response){
                var jsonObj = JSON.parse(response.body);
                if(DontDestroy.JSONLoader == null)
                    DontDestroy.JSONLoader = new THREE.JSONLoader();
                var model = DontDestroy.JSONLoader.parse(jsonObj, RoboVisualizer.TexturesPath, function(){
                    if(model.geometry != undefined && model.materials != null)
                    {
                        var material = new THREE.MultiMaterial(model.materials);
                        DestroyOnJourney.Terrain = new THREE.Mesh(model.geometry, material);
                        for(var prop in material.materials) {
                            if(material.materials[prop] != undefined && material.materials[prop].map != null) {
                                material.materials[prop].map.wrapS = THREE.RepeatWrapping;
                                material.materials[prop].map.wrapT = THREE.RepeatWrapping;
                                tex.push(material.materials[prop].map.clone());
                                if(tex[tex_sequence] != undefined)
                                    tex[tex_sequence].needsUpdate = true;
                                tex_sequence++;
                            }
                        }
                        DestroyOnJourney.Terrain.traverse ( function (child) {
                            if (child instanceof THREE.Mesh) {
                                child.castShadow = true;
                                child.receiveShadow = true;
                            }
                        });
                        DestroyOnJourney.Terrain.position.x = 5.5;
                        DestroyOnJourney.Terrain.position.y = 0;
                        DestroyOnJourney.Terrain.position.z = -5.5;
                        DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
                        materialCounter++;
                        if(materialCounter == model.materials.length)
                            callback();
                    }
                    else
                    {
                        var errorType = "LoadingError";
                        var errorMsg = "Error loading \"" + assetPath + "\"";
                        var caller = arguments.callee.name;
                        var err = new VisualizerError(errorType, errorMsg, caller);
                        visualizer.SetError(err);
                        callback(err);
                    }
                });
            });
        }
        else
        {
            DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
            callback();
        }
    },
    LateInitialize: function()
    {
        // if(DestroyOnJourney.particleSystem == undefined)
        // {
        //     if(DontDestroy.ParticlesCam == undefined)
        //         DontDestroy.ParticlesCam = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        //     if(DontDestroy.ParticlesScene == undefined)
        //         DontDestroy.ParticlesScene = new THREE.Scene();
        //     Particles.ParticlSystem.push(new SPE.Group({
        //         texture: {
        //             value: THREE.ImageUtils.loadTexture(RoboVisualizer.TexturesPath + 'ballons.png')
        //         },
        //         blending: THREE.NormalBlending,
        //         fog: true
        //     }));
        //
        //     emitter = new SPE.Emitter({
        //         particleCount: 1,
        //         maxAge: {
        //             value: 4,
        //         },
        //         position: {
        //             value: new THREE.Vector3( -10, 0, -40 ),
        //             spread: new THREE.Vector3( 80, 0, 0 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 3, 0 )
        //         },
        //         wiggle: {
        //             spread: 20
        //         },
        //         size: {
        //             value: 10,
        //             spread: 1
        //         },
        //         opacity: {
        //             value: [ 1, 1, 1 ]
        //         },
        //         color: {
        //             value: new THREE.Color( 1, 0, 0 ),
        //         },
        //         angle: {
        //             value: [ 0, Math.PI * 0.125 ]
        //         }
        //     });
        //
        //     emitter1 = new SPE.Emitter({
        //         particleCount: 1,
        //         maxAge: {
        //             value: 4,
        //         },
        //         position: {
        //             value: new THREE.Vector3( -10, 0, -40 ),
        //             spread: new THREE.Vector3( 80, 0, 0 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 3, 0 )
        //         },
        //         wiggle: {
        //             spread: 20
        //         },
        //         size: {
        //             value: 10,
        //             spread: 1
        //         },
        //         opacity: {
        //             value: [ 1, 1, 1 ]
        //         },
        //         color: {
        //             value: new THREE.Color( 0, 1, 0 ),
        //         },
        //         angle: {
        //             value: [ 0, Math.PI * 0.125 ]
        //         }
        //     });
        //
        //     emitter2 = new SPE.Emitter({
        //         particleCount: 1,
        //         maxAge: {
        //             value: 4,
        //         },
        //         position: {
        //             value: new THREE.Vector3( -10, 0, -40 ),
        //             spread: new THREE.Vector3( 80, 0, 0 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 3, 0 )
        //         },
        //         wiggle: {
        //             spread: 20
        //         },
        //         size: {
        //             value: 10,
        //             spread: 1
        //         },
        //         opacity: {
        //             value: [ 1, 1, 1 ]
        //         },
        //         color: {
        //             value: new THREE.Color( 0, 0, 1 ),
        //         },
        //         angle: {
        //             value: [ 0, Math.PI * 0.125 ]
        //         }
        //     });
        //
        //     emitter3 = new SPE.Emitter({
        //         particleCount: 1,
        //         maxAge: {
        //             value: 4,
        //         },
        //         position: {
        //             value: new THREE.Vector3( -10, 0, -40 ),
        //             spread: new THREE.Vector3( 80, 0, 0 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 3, 0 )
        //         },
        //         wiggle: {
        //             spread: 20
        //         },
        //         size: {
        //             value: 10,
        //             spread: 1
        //         },
        //         opacity: {
        //             value: [ 1, 1, 1 ]
        //         },
        //         color: {
        //             value: new THREE.Color( 1, 1, 0 ),
        //         },
        //         angle: {
        //             value: [ 0, Math.PI * 0.125 ]
        //         }
        //     });
        //
        //     Particles.ParticlSystem[0].addEmitter( emitter );
        //     Particles.ParticlSystem[0].addEmitter( emitter1 );
        //     Particles.ParticlSystem[0].addEmitter( emitter2 );
        //     Particles.ParticlSystem[0].addEmitter( emitter3 );
        //     DontDestroy.ParticlesScene.add( Particles.ParticlSystem[0].mesh );
        //     Particles.clock = new THREE.Clock();
        // }
        // Particles.isReady = true;
    }
}

Scenes["birthday2d"] = {
    Initialize: function(divID, callback)
    {
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        if(DestroyOnJourney.BGScene == undefined)
            DestroyOnJourney.BGScene = new THREE.Scene();
        if(DontDestroy.BGCamera == undefined)
            DontDestroy.BGCamera = new THREE.OrthographicCamera(-Configurations.RendererSize_x/2, Configurations.RendererSize_x/2, Configurations.RendererSize_y/2, -Configurations.RendererSize_y/2, 0, 30);
        if(DontDestroy.TextureLoader == undefined)
            DontDestroy.TextureLoader = new THREE.TextureLoader();

        // setup LCD
        lcd = new LCD();
        // lights
        DestroyOnJourney.ambientLight = new THREE.AmbientLight(0xffffff, 1.8); // soft white light
        DontDestroy.mainScene.add( DestroyOnJourney.ambientLight );
        
        // DestroyOnJourney.spotLight = new THREE.SpotLight(0xffffff, 0.5, 250, 0.21, 0, 0);
        // DestroyOnJourney.spotLight.position.set(60, 90, -100 );
        // //Set up shadow properties for the light
        // DestroyOnJourney.spotLight.castShadow = true;
        // DestroyOnJourney.spotLight.shadowCameraVisible = true;
        // DestroyOnJourney.spotLight.shadow.mapSize.width = 2048;  // default
        // DestroyOnJourney.spotLight.shadow.mapSize.height = 2048; // default
        // DestroyOnJourney.spotLight.shadow.camera.near = 0.5;       // default
        // DestroyOnJourney.spotLight.shadow.camera.far = 50      // default
        // DestroyOnJourney.spotLight.shadowDarkness = 1;
        // DestroyOnJourney.spotLight.target.position.set(5, 3, 0);
        // DestroyOnJourney.spotLight.bias = 0.001;
        // DontDestroy.mainScene.add( DestroyOnJourney.spotLight );
        
        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );
        
        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var spriteMap = DontDestroy.TextureLoader.load(RoboVisualizer.TexturesPath + "birthday2d.jpg",
            function(texture)
            {
                texture.needsUpdate = true;
                var spriteMaterial = new THREE.SpriteMaterial({map: texture});
                var sprite = new THREE.Sprite(spriteMaterial);
                sprite.scale.set(Configurations.RendererSize_x, Configurations.RendererSize_y, 0.1);
                sprite.renderOrder = 1;
                DestroyOnJourney.BGScene.add(sprite);
                RoboVisualizer.gameFlags.showLoadingScene = false;
                callback();
            },
            function()
            {},
            function loadingScene_Initialize()
            {
                var errorType = "LoadingError";
                var errorMsg = "Error loading \"" + RoboVisualizer.TexturesPath + "birthday2d.jpg" + "\"";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                visualizer.SetError(err);
                callback(err);
            }
        );
    },
    LateInitialize: function()
    {
        // if(DestroyOnJourney.particleSystem == undefined)
        // {
        //     if(DontDestroy.ParticlesCam == undefined)
        //         DontDestroy.ParticlesCam = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        //     if(DontDestroy.ParticlesScene == undefined)
        //         DontDestroy.ParticlesScene = new THREE.Scene();
        //     Particles.ParticlSystem.push(new SPE.Group({
        //         texture: {
        //             value: THREE.ImageUtils.loadTexture(RoboVisualizer.TexturesPath + 'ballons.png')
        //         },
        //         blending: THREE.NormalBlending,
        //         fog: true
        //     }));
        //
        //     emitter = new SPE.Emitter({
        //         particleCount: 1,
        //         maxAge: {
        //             value: 4,
        //         },
        //         position: {
        //             value: new THREE.Vector3( -10, 0, -40 ),
        //             spread: new THREE.Vector3( 80, 0, 0 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 3, 0 )
        //         },
        //         wiggle: {
        //             spread: 20
        //         },
        //         size: {
        //             value: 10,
        //             spread: 1
        //         },
        //         opacity: {
        //             value: [ 1, 1, 1 ]
        //         },
        //         color: {
        //             value: new THREE.Color( 1, 0, 0 ),
        //         },
        //         angle: {
        //             value: [ 0, Math.PI * 0.125 ]
        //         }
        //     });
        //
        //     emitter1 = new SPE.Emitter({
        //         particleCount: 1,
        //         maxAge: {
        //             value: 4,
        //         },
        //         position: {
        //             value: new THREE.Vector3( -10, 0, -40 ),
        //             spread: new THREE.Vector3( 80, 0, 0 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 3, 0 )
        //         },
        //         wiggle: {
        //             spread: 20
        //         },
        //         size: {
        //             value: 10,
        //             spread: 1
        //         },
        //         opacity: {
        //             value: [ 1, 1, 1 ]
        //         },
        //         color: {
        //             value: new THREE.Color( 0, 1, 0 ),
        //         },
        //         angle: {
        //             value: [ 0, Math.PI * 0.125 ]
        //         }
        //     });
        //
        //     emitter2 = new SPE.Emitter({
        //         particleCount: 1,
        //         maxAge: {
        //             value: 4,
        //         },
        //         position: {
        //             value: new THREE.Vector3( -10, 0, -40 ),
        //             spread: new THREE.Vector3( 80, 0, 0 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 3, 0 )
        //         },
        //         wiggle: {
        //             spread: 20
        //         },
        //         size: {
        //             value: 10,
        //             spread: 1
        //         },
        //         opacity: {
        //             value: [ 1, 1, 1 ]
        //         },
        //         color: {
        //             value: new THREE.Color( 0, 0, 1 ),
        //         },
        //         angle: {
        //             value: [ 0, Math.PI * 0.125 ]
        //         }
        //     });
        //
        //     emitter3 = new SPE.Emitter({
        //         particleCount: 1,
        //         maxAge: {
        //             value: 4,
        //         },
        //         position: {
        //             value: new THREE.Vector3( -10, 0, -40 ),
        //             spread: new THREE.Vector3( 80, 0, 0 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 3, 0 )
        //         },
        //         wiggle: {
        //             spread: 20
        //         },
        //         size: {
        //             value: 10,
        //             spread: 1
        //         },
        //         opacity: {
        //             value: [ 1, 1, 1 ]
        //         },
        //         color: {
        //             value: new THREE.Color( 1, 1, 0 ),
        //         },
        //         angle: {
        //             value: [ 0, Math.PI * 0.125 ]
        //         }
        //     });
        //
        //     Particles.ParticlSystem[0].addEmitter( emitter );
        //     Particles.ParticlSystem[0].addEmitter( emitter1 );
        //     Particles.ParticlSystem[0].addEmitter( emitter2 );
        //     Particles.ParticlSystem[0].addEmitter( emitter3 );
        //     DontDestroy.ParticlesScene.add( Particles.ParticlSystem[0].mesh );
        //     Particles.clock = new THREE.Clock();
        // }
        // Particles.isReady = true;
    }
}

Scenes["golden"] = {
    Initialize: function(divID, callback)
    {
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        DontDestroy.mainScene.fog = new THREE.Fog(0x9a5232, -60, 180);
        
        // setup LCD
        lcd = new LCD();

        //lights
        var ambientLight = new THREE.AmbientLight(0xffffff, 1.5); // soft white light
        DontDestroy.mainScene.add( ambientLight );
        
        var spotLight = new THREE.SpotLight(0xffffff, 1, 250, 0.21, 0, 0);
        spotLight.position.set(60, 90, -100 );
        //Set up shadow properties for the light
        spotLight.castShadow = true;
        spotLight.shadowCameraVisible = true;
        spotLight.shadow.mapSize.width = 2048;  // default
        spotLight.shadow.mapSize.height = 2048; // default
        spotLight.target.position.set(5, 3, 0);
        spotLight.shadow.camera.near = 0.5;       // default
        spotLight.shadowDarkness = 1;
        spotLight.bias = 0.001;
        DontDestroy.mainScene.add( spotLight );

        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );

        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var tex = new Array();
        var tex_sequence = 0;
        var numOfMaterials = 5;
        var materialCounter = 0;
        var assetPath = RoboVisualizer.AssetsPath + 'assets/models/golden_ terrain';
        if(DestroyOnJourney.Terrain == null || DestroyOnJourney.Terrain == undefined)
        {
            loadJSON(assetPath + ".js", function(response){
                var jsonObj = JSON.parse(response.body);
                if(DontDestroy.JSONLoader == null)
                    DontDestroy.JSONLoader = new THREE.JSONLoader();
                var model = DontDestroy.JSONLoader.parse(jsonObj, RoboVisualizer.TexturesPath, function(){
                    if(model.geometry != undefined && model.materials != null)
                    {
                        var material = new THREE.MultiMaterial(model.materials);
                        DestroyOnJourney.Terrain = new THREE.Mesh(model.geometry, material);
                        for(var prop in material.materials) {
                            if(material.materials[prop] != undefined && material.materials[prop].map != null) {
                                material.materials[prop].map.wrapS = THREE.RepeatWrapping;
                                material.materials[prop].map.wrapT = THREE.RepeatWrapping;
                                tex.push(material.materials[prop].map.clone());
                                if(tex[tex_sequence] != undefined)
                                    tex[tex_sequence].needsUpdate = true;
                                tex_sequence++;
                            }
                        }
                        DestroyOnJourney.Terrain.traverse ( function (child) {
                            if (child instanceof THREE.Mesh) {
                                child.castShadow = true;
                                child.receiveShadow = true;
                            }
                        });
                        DestroyOnJourney.Terrain.position.x = 5.52;
                        DestroyOnJourney.Terrain.position.y = 0.05;
                        DestroyOnJourney.Terrain.position.z = -5.47;
                        DestroyOnJourney.Terrain.scale.x = DestroyOnJourney.Terrain.scale.y = DestroyOnJourney.Terrain.scale.z = 1;
                        DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
                        materialCounter++;
                        if(materialCounter == numOfMaterials)
                            callback();
                    }
                    else
                    {
                        var errorType = "LoadingError";
                        var errorMsg = "Error loading \"" + assetPath + "\"";
                        var caller = arguments.callee.name;
                        var err = new VisualizerError(errorType, errorMsg, caller);
                        visualizer.SetError(err);
                        callback(err);
                    }
                });
            });
        }
        else
        {
            DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
            callback();
        }
    }
}

Scenes["golden2d"] = {
    Initialize: function(divID, callback)
    {
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        if(DestroyOnJourney.BGScene == undefined)
            DestroyOnJourney.BGScene = new THREE.Scene();
        if(DontDestroy.BGCamera == undefined)
            DontDestroy.BGCamera = new THREE.OrthographicCamera(-Configurations.RendererSize_x/2, Configurations.RendererSize_x/2, Configurations.RendererSize_y/2, -Configurations.RendererSize_y/2, 0, 30);
        if(DontDestroy.TextureLoader == undefined)
            DontDestroy.TextureLoader = new THREE.TextureLoader();

        // setup LCD
        lcd = new LCD();
        //lights
        DestroyOnJourney.ambientLight = new THREE.AmbientLight(0xffffff, 1.5); // soft white light
        DontDestroy.mainScene.add( DestroyOnJourney.ambientLight );
       
        // DestroyOnJourney.spotLight = new THREE.SpotLight(0xffffff, 1, 250, 0.21, 0, 0);
        // DestroyOnJourney.spotLight.position.set(60, 90, -100 );
        // //Set up shadow properties for the light
        // DestroyOnJourney.spotLight.castShadow = true;
        // DestroyOnJourney.spotLight.shadowCameraVisible = true;
        // DestroyOnJourney.spotLight.shadow.mapSize.width = 2048;  // default
        // DestroyOnJourney.spotLight.shadow.mapSize.height = 2048; // default
        // DestroyOnJourney.spotLight.target.position.set(5, 3, 0);
        // DestroyOnJourney.spotLight.shadow.camera.near = 0.5;       // default
        // DestroyOnJourney.spotLight.shadowDarkness = 1;
        // DestroyOnJourney.spotLight.bias = 0.001;
        // DontDestroy.mainScene.add( DestroyOnJourney.spotLight );
        
        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );
        
        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var spriteMap = DontDestroy.TextureLoader.load(RoboVisualizer.TexturesPath + "golden2d.jpg",
            function(texture)
            {
                texture.needsUpdate = true;
                var spriteMaterial = new THREE.SpriteMaterial({map: texture});
                var sprite = new THREE.Sprite(spriteMaterial);
                sprite.scale.set(Configurations.RendererSize_x, Configurations.RendererSize_y, 0.1);
                sprite.renderOrder = 1;
                DestroyOnJourney.BGScene.add(sprite);
                RoboVisualizer.gameFlags.showLoadingScene = false;
                callback();
            },
            function()
            {},
            function loadingScene_Initialize()
            {
                var errorType = "LoadingError";
                var errorMsg = "Error loading \"" + RoboVisualizer.TexturesPath + "golden2d.jpg" + "\"";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                visualizer.SetError(err);
                callback(err);
            }
        );
    },
    LateInitialize: function()
    {
        // if(DestroyOnJourney.particleSystem == undefined)
        // {
        //     if(DontDestroy.ParticlesCam == undefined)
        //         DontDestroy.ParticlesCam = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        //     if(DontDestroy.ParticlesScene == undefined)
        //         DontDestroy.ParticlesScene = new THREE.Scene();
        //
        //     Particles.ParticlSystem.push(new SPE.Group({
        //         texture: {
        //             value: THREE.ImageUtils.loadTexture(RoboVisualizer.TexturesPath + 'blue_spark.png')
        //         },
        //         blending: THREE.AdditiveBlending
        //     }));
        //
        //     Particles.ParticlSystem.push(new SPE.Group({
        //         texture: {
        //             value: THREE.ImageUtils.loadTexture(RoboVisualizer.TexturesPath + 'gold_spark.png')
        //         },
        //         blending: THREE.AdditiveBlending
        //     }));
        //
        //     emitter = {
        //         maxAge: {
        //             value: 0.5,
        //         },
        //         position: {
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 0, 0 ),
        //             spread: new THREE.Vector3( 0, 0, 0 )
        //         },
        //         wiggle: {
        //             spread: 0
        //         },
        //         size: {
        //             value: 3
        //         },
        //         color: {
        //             value: new THREE.Color( 0.5, 0, 1 ),
        //         }
        //     };
        //
        //     emitter1 = {
        //         maxAge: {
        //             value: 0.5,
        //         },
        //         position: {
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 0, 0 ),
        //             spread: new THREE.Vector3( 0, 0, 0 )
        //         },
        //         wiggle: {
        //             spread: 0
        //         },
        //         opacity: {
        //             value: [ 0.5, 0.5, 0.5]
        //         },
        //         size: {
        //             value: 2
        //         },
        //         color: {
        //             value: new THREE.Color( 1, 1, 1 ),
        //         }
        //     };
        //     Particles.positions =[];
        //     //blue
        //     Particles.positions.push(new THREE.Vector3(-7.7,5,-10));
        //     Particles.positions.push(new THREE.Vector3(-2.7,3,-7));
        //     Particles.positions.push(new THREE.Vector3(7.2,5,-10));
        //     Particles.positions.push(new THREE.Vector3(16.2,3,-20));
        //     //gold
        //     Particles.positions.push(new THREE.Vector3(0, 3, -7));
        //     Particles.positions.push(new THREE.Vector3(5, 4, -10));
        //     Particles.positions.push(new THREE.Vector3(10, 3, -10));
        //     Particles.positions.push(new THREE.Vector3(14.5, 2.7, -7));
        //
        //     Particles.ParticlSystem[0].addPool( 4, emitter, false );
        //     Particles.ParticlSystem[1].addPool( 4, emitter1, false );
        //     DontDestroy.ParticlesScene.add( Particles.ParticlSystem[0].mesh );
        //     DontDestroy.ParticlesScene.add( Particles.ParticlSystem[1].mesh );
        //     Particles.clock = new THREE.Clock();
        // }
        // Particles.isReady = true;
        // Particles.clock.start();
    }
}

Scenes["moon"] = {
    Initialize: function(divID, callback)
    {
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        
        // setup LCD
        lcd = new LCD();

        //lights
        var ambientLight = new THREE.AmbientLight(0xffffff, 1.5); // soft white light
        DontDestroy.mainScene.add( ambientLight );
       
        var spotLight = new THREE.SpotLight(0xffffff, 0.5, 250, 0.21, 0, 0);
        spotLight.position.set(60, 90, -100 );
        //Set up shadow properties for the light
        spotLight.castShadow = true;
        spotLight.shadowCameraVisible = true;
        spotLight.shadow.mapSize.width = 2048;  // default
        spotLight.shadow.mapSize.height = 2048; // default
        spotLight.target.position.set(5, 3, 0);
        spotLight.shadow.camera.near = 0.5;       // default
        spotLight.shadowDarkness = 1;
        spotLight.bias = 0.001;
        DontDestroy.mainScene.add( spotLight );

        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );

        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var tex = new Array();
        var tex_sequence = 0;
        var numOfMaterials = 6;
        var materialCounter = 0;
        var assetPath = RoboVisualizer.AssetsPath + 'assets/models/moon_terrain';
        if(DestroyOnJourney.Terrain == null || DestroyOnJourney.Terrain == undefined)
        {
            loadJSON(assetPath + ".js", function(response){
                var jsonObj = JSON.parse(response.body);
                if(DontDestroy.JSONLoader == null)
                    DontDestroy.JSONLoader = new THREE.JSONLoader();
                var model = DontDestroy.JSONLoader.parse(jsonObj, RoboVisualizer.TexturesPath, function(){
                    if(model.geometry != undefined && model.materials != null)
                    {
                        var material = new THREE.MultiMaterial(model.materials);
                        DestroyOnJourney.Terrain = new THREE.Mesh(model.geometry, material);
                        for(var prop in material.materials) {
                            if(material.materials[prop] != undefined && material.materials[prop].map != null) {
                                material.materials[prop].map.wrapS = THREE.RepeatWrapping;
                                material.materials[prop].map.wrapT = THREE.RepeatWrapping;
                                tex.push(material.materials[prop].map.clone());
                                if(tex[tex_sequence] != undefined)
                                    tex[tex_sequence].needsUpdate = true;
                                tex_sequence++;
                            }
                        }
                        DestroyOnJourney.Terrain.traverse ( function (child) {
                            if (child instanceof THREE.Mesh) {
                                child.castShadow = true;
                                child.receiveShadow = true;
                            }
                        });
                        DestroyOnJourney.Terrain.position.x = 5.54;
                        DestroyOnJourney.Terrain.position.y = 0;
                        DestroyOnJourney.Terrain.position.z = -5.5;
                        DestroyOnJourney.Terrain.scale.x = DestroyOnJourney.Terrain.scale.y = 1.3;
                        DestroyOnJourney.Terrain.scale.z = 1.4;
                        DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
                        materialCounter++;
                        if(materialCounter == numOfMaterials)
                            callback();
                    }
                    else
                    {
                        var errorType = "LoadingError";
                        var errorMsg = "Error loading \"" + assetPath + "\"";
                        var caller = arguments.callee.name;
                        var err = new VisualizerError(errorType, errorMsg, caller);
                        visualizer.SetError(err);
                        callback(err);
                    }
                });
            });
        }
        else
        {
            DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
            callback();
        }
    }
}

Scenes["moon2d"] = {
    Initialize: function(divID, callback)
    {
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        if(DestroyOnJourney.BGScene == undefined)
            DestroyOnJourney.BGScene = new THREE.Scene();
        if(DontDestroy.BGCamera == undefined)
            DontDestroy.BGCamera = new THREE.OrthographicCamera(-Configurations.RendererSize_x/2, Configurations.RendererSize_x/2, Configurations.RendererSize_y/2, -Configurations.RendererSize_y/2, 0, 30);
        if(DontDestroy.TextureLoader == undefined)
            DontDestroy.TextureLoader = new THREE.TextureLoader();

        // setup LCD
        lcd = new LCD();
        //lights
        DestroyOnJourney.ambientLight = new THREE.AmbientLight(0xffffff, 1.5); // soft white light
        DontDestroy.mainScene.add( DestroyOnJourney.ambientLight );
        
        // DestroyOnJourney.spotLight = new THREE.SpotLight(0xffffff, 0.5, 250, 0.21, 0, 0);
        // DestroyOnJourney.spotLight.position.set(60, 90, -100 );
        // //Set up shadow properties for the light
        // DestroyOnJourney.spotLight.castShadow = true;
        // DestroyOnJourney.spotLight.shadowCameraVisible = true;
        // DestroyOnJourney.spotLight.shadow.mapSize.width = 2048;  // default
        // DestroyOnJourney.spotLight.shadow.mapSize.height = 2048; // default
        // DestroyOnJourney.spotLight.target.position.set(5, 3, 0);
        // DestroyOnJourney.spotLight.shadow.camera.near = 0.5;       // default
        // DestroyOnJourney.spotLight.shadowDarkness = 1;
        // DestroyOnJourney.spotLight.bias = 0.001;
        // DontDestroy.mainScene.add( DestroyOnJourney.spotLight );
        
        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );
        
        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var spriteMap = DontDestroy.TextureLoader.load(RoboVisualizer.TexturesPath + "moon2d.jpg",
            function(texture)
            {
                texture.needsUpdate = true;
                var spriteMaterial = new THREE.SpriteMaterial({map: texture});
                var sprite = new THREE.Sprite(spriteMaterial);
                sprite.scale.set(Configurations.RendererSize_x, Configurations.RendererSize_y, 0.1);
                sprite.renderOrder = 1;
                DestroyOnJourney.BGScene.add(sprite);
                RoboVisualizer.gameFlags.showLoadingScene = false;
                callback();
            },
            function()
            {},
            function loadingScene_Initialize()
            {
                var errorType = "LoadingError";
                var errorMsg = "Error loading \"" + RoboVisualizer.TexturesPath + "moon2d.jpg" + "\"";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                visualizer.SetError(err);
                callback(err);
            }
        );
    },
    LateInitialize: function()
    {
        // if(DestroyOnJourney.particleSystem == undefined)
        // {
        //     if(DontDestroy.ParticlesCam == undefined)
        //         DontDestroy.ParticlesCam = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        //     if(DontDestroy.ParticlesScene == undefined)
        //         DontDestroy.ParticlesScene = new THREE.Scene();
        //
        //     Particles.ParticlSystem.push(new SPE.Group({
        //         texture: {
        //             value: THREE.ImageUtils.loadTexture(RoboVisualizer.TexturesPath + 'star1.png')
        //         },
        //         blending: THREE.NormalBlending,
        //         fog: true
        //     }));
        //
        //     emitter = new SPE.Emitter({
        //         particleCount: 100,
        //         maxAge: {
        //             value: 4,
        //         },
        //         position: {
        //             value: new THREE.Vector3( -10, 15, -20 ),
        //             spread: new THREE.Vector3( 80, 20, 2 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 0, 0 ),
        //             spread: new THREE.Vector3( 0, 0, 0 )
        //         },
        //         wiggle: {
        //             spread: 0
        //         },
        //         size: {
        //             value: 10,
        //             spread: 5
        //         },
        //         opacity: {
        //             value: [ Math.random(), Math.random(), Math.random()]
        //         },
        //         color: {
        //             value: new THREE.Color( 1, 1, 1 ),
        //             spread: new THREE.Color( 0.1, 0.1, 0.1 )
        //         },
        //         angle: {
        //             value: [ 0, 0 ]
        //         }
        //     });
        //
        //     Particles.ParticlSystem[0].addEmitter( emitter );
        //     DontDestroy.ParticlesScene.add( Particles.ParticlSystem[0].mesh );
        //     Particles.clock = new THREE.Clock();
        // }
        // Particles.isReady = true;
    }
}

Scenes["volcanic"] = {
    Initialize: function(divID, callback)
    {
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        DontDestroy.mainScene.fog = new THREE.Fog(0x1a2c42, 0, 150);
        // setup LCD
        lcd = new LCD();

        //lights
        var ambientLight = new THREE.AmbientLight(0xffffff, 1.5);
        DontDestroy.mainScene.add( ambientLight );
        
        var spotLight = new THREE.SpotLight(0xffffff, 0.5, 250, 0.21, 0, 0);
        spotLight.position.set(60, 90, -100 );
        //Set up shadow properties for the light
        spotLight.castShadow = true;
        spotLight.shadowCameraVisible = true;
        spotLight.shadow.mapSize.width = 2048;  
        spotLight.shadow.mapSize.height = 2048; 
        spotLight.target.position.set(5, 3, 0);
        spotLight.shadow.camera.near = 0.5;
        spotLight.shadowDarkness = 1;
        spotLight.bias = 0.001;
        DontDestroy.mainScene.add( spotLight );

        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );

        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var tex = new Array();
        var tex_sequence = 0;
        var numOfMaterials = 4;
        var materialCounter = 0;
        var assetPath = RoboVisualizer.AssetsPath + 'assets/models/volcano_terrain';
        if(DestroyOnJourney.Terrain == null || DestroyOnJourney.Terrain == undefined)
        {
            loadJSON(assetPath + ".js", function(response){
                var jsonObj = JSON.parse(response.body);
                if(DontDestroy.JSONLoader == null)
                    DontDestroy.JSONLoader = new THREE.JSONLoader();
                var model = DontDestroy.JSONLoader.parse(jsonObj, RoboVisualizer.TexturesPath, function(){
                    if(model.geometry != undefined && model.materials != null)
                    {
                        var material = new THREE.MultiMaterial(model.materials);
                        DestroyOnJourney.Terrain = new THREE.Mesh(model.geometry, material);
                        for(var prop in material.materials) {
                            if(material.materials[prop] != undefined && material.materials[prop].map != null) {
                                material.materials[prop].map.wrapS = THREE.RepeatWrapping;
                                material.materials[prop].map.wrapT = THREE.RepeatWrapping;
                                tex.push(material.materials[prop].map.clone());
                                if(tex[tex_sequence] != undefined)
                                    tex[tex_sequence].needsUpdate = true;
                                tex_sequence++;
                            }
                        }
                        DestroyOnJourney.Terrain.traverse ( function (child) {
                            if (child instanceof THREE.Mesh) {
                                child.castShadow = true;
                                child.receiveShadow = true;
                            }
                        });
                        DestroyOnJourney.Terrain.position.x = 5.54;
                        DestroyOnJourney.Terrain.position.y = -0.01;
                        DestroyOnJourney.Terrain.position.z = -5.5;
                        DestroyOnJourney.Terrain.scale.x = DestroyOnJourney.Terrain.scale.y = DestroyOnJourney.Terrain.scale.z = 0.953;
                        DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
                        materialCounter++;
                        if(materialCounter == numOfMaterials)
                            callback();
                    }
                    else
                    {
                        var errorType = "LoadingError";
                        var errorMsg = "Error loading \"" + assetPath + "\"";
                        var caller = arguments.callee.name;
                        var err = new VisualizerError(errorType, errorMsg, caller);
                        visualizer.SetError(err);
                        callback(err);
                    }
                });
            });
        }
        else
        {
            DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
            callback();
        }   
    }
}

Scenes["volcanic2d"] = {
    Initialize: function(divID, callback)
    {
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        if(DestroyOnJourney.BGScene == undefined)
            DestroyOnJourney.BGScene = new THREE.Scene();
        if(DontDestroy.BGCamera == undefined)
            DontDestroy.BGCamera = new THREE.OrthographicCamera(-Configurations.RendererSize_x/2, Configurations.RendererSize_x/2, Configurations.RendererSize_y/2, -Configurations.RendererSize_y/2, 0, 30);
        if(DontDestroy.TextureLoader == undefined)
            DontDestroy.TextureLoader = new THREE.TextureLoader();

        // setup LCD
        lcd = new LCD();
        //lights
        DestroyOnJourney.ambientLight = new THREE.AmbientLight(0xffffff, 1.5);
        DontDestroy.mainScene.add( DestroyOnJourney.ambientLight );
       
        // DestroyOnJourney.spotLight = new THREE.SpotLight(0xffffff, 0.5, 250, 0.21, 0, 0);
        // DestroyOnJourney.spotLight.position.set(60, 90, -100 );
        // //Set up shadow properties for the light
        // DestroyOnJourney.spotLight.castShadow = true;
        // DestroyOnJourney.spotLight.shadowCameraVisible = true;
        // DestroyOnJourney.spotLight.shadow.mapSize.width = 2048;  
        // DestroyOnJourney.spotLight.shadow.mapSize.height = 2048; 
        // DestroyOnJourney.spotLight.target.position.set(5, 3, 0);
        // DestroyOnJourney.spotLight.shadow.camera.near = 0.5;
        // DestroyOnJourney.spotLight.shadowDarkness = 1;
        // DestroyOnJourney.spotLight.bias = 0.001;
        // DontDestroy.mainScene.add( DestroyOnJourney.spotLight );
        
        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );
        
        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var spriteMap = DontDestroy.TextureLoader.load(RoboVisualizer.TexturesPath + "volcanic2d.jpg",
            function(texture)
            {
                texture.needsUpdate = true;
                var spriteMaterial = new THREE.SpriteMaterial({map: texture});
                var sprite = new THREE.Sprite(spriteMaterial);
                sprite.scale.set(Configurations.RendererSize_x, Configurations.RendererSize_y, 0.1);
                sprite.renderOrder = 1;
                DestroyOnJourney.BGScene.add(sprite);
                RoboVisualizer.gameFlags.showLoadingScene = false;
                callback();
            },
            function()
            {},
            function loadingScene_Initialize()
            {
                var errorType = "LoadingError";
                var errorMsg = "Error loading \"" + RoboVisualizer.TexturesPath + "volcanic2d.jpg" + "\"";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                visualizer.SetError(err);
                callback(err);
            }
        );
    },
    LateInitialize: function()
    {
        // if(DestroyOnJourney.particleSystem == undefined)
        // {
        //     if(DontDestroy.ParticlesCam == undefined)
        //         DontDestroy.ParticlesCam = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        //     if(DontDestroy.ParticlesScene == undefined)
        //         DontDestroy.ParticlesScene = new THREE.Scene();
        //     Particles.ParticlSystem = [];
        //     Particles.ParticlSystem.push(new SPE.Group({
        //         texture: {
        //             value: THREE.ImageUtils.loadTexture(RoboVisualizer.TexturesPath + 'spark.png')
        //         },
        //         blending: THREE.NormalBlending,
        //         fog: true
        //     }));
        //
        //     Particles.ParticlSystem.push(new SPE.Group({
        //         texture: {
        //             value: THREE.ImageUtils.loadTexture(RoboVisualizer.TexturesPath + 'smokeparticle.png')
        //         },
        //         blending: THREE.NormalBlending,
        //         fog: true
        //     }));
        //
        //     emitter = new SPE.Emitter({
        //         particleCount: 300,
        //         maxAge: {
        //             value: 3,
        //         },
        //         position: {
        //             value: new THREE.Vector3( 0, -10, -40 ),
        //             spread: new THREE.Vector3( 60, -10, -10 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( Math.random()*3,  Math.random()*5,  Math.random()*5 ),
        //         },
        //         wiggle: {
        //             spread: 30
        //         },
        //         size: {
        //             value: 0.5,
        //             spread: 5
        //         },
        //         opacity: {
        //             value: [ 0.9, 0.9, 0.9]
        //         },
        //         color: {
        //             value: new THREE.Color( 1, 0, 0 ),
        //             spread: new THREE.Color( 0.1, 0.1, 0.1 )
        //         },
        //         angle: {
        //             value: [ 0, Math.PI * 0.125 ]
        //         }
        //     });
        //
        //     emitter1 = new SPE.Emitter({
        //         particleCount: 10,
        //         maxAge: {
        //             value: 3,
        //         },
        //         position: {
        //             value: new THREE.Vector3( 17, 7, -20 ),
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 0.5, 0 )
        //         },
        //         wiggle: {
        //             spread: 0
        //         },
        //         size: {
        //             value: 10,
        //         },
        //         opacity: {
        //             value: [ 0.5, 0.5, 0.5]
        //         },
        //         color: {
        //             value: new THREE.Color( 0.5, 0.5, 0.5 ),
        //             spread: new THREE.Color( 0.1, 0.1, 0.1 )
        //         },
        //         angle: {
        //             value: [ 0, 0]
        //         }
        //     });
        //
        //     emitter2 = new SPE.Emitter({
        //         particleCount: 10,
        //         maxAge: {
        //             value: 3,
        //         },
        //         position: {
        //             value: new THREE.Vector3( -6, 7, -20 ),
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( 0, 0.5, 0 )
        //         },
        //         wiggle: {
        //             spread: 0
        //         },
        //         size: {
        //             value: 10,
        //         },
        //         opacity: {
        //             value: [ 0.5, 0.5, 0.5]
        //         },
        //         color: {
        //             value: new THREE.Color( 0.5, 0.5, 0.5 ),
        //             spread: new THREE.Color( 0.1, 0.1, 0.1 )
        //         },
        //         angle: {
        //             value: [ 0, 0]
        //         }
        //     });
        //
        //     Particles.ParticlSystem[0].addEmitter( emitter );
        //     Particles.ParticlSystem[1].addEmitter( emitter1 );
        //     Particles.ParticlSystem[1].addEmitter( emitter2 );
        //     DontDestroy.ParticlesScene.add( Particles.ParticlSystem[0].mesh );
        //     DontDestroy.ParticlesScene.add( Particles.ParticlSystem[1].mesh );
        //     Particles.clock = new THREE.Clock();
        // }
        // Particles.isReady = true;
    }
}

Scenes["iceworld"] = {
    Initialize: function(divID, callback)
    {
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();

        // setup LCD
        lcd = new LCD();

        //lights
        var ambientLight = new THREE.AmbientLight(0xffffff, 0.85);
        DontDestroy.mainScene.add( ambientLight );
       
        var spotLight = new THREE.SpotLight(0xffffff, 0.5, 250, 0.21, 0, 0);
        spotLight.position.set(60, 90, -100 );
        //Set up shadow properties for the light
        spotLight.castShadow = true;
        spotLight.shadowCameraVisible = true;
        spotLight.shadow.mapSize.width = 2048;  
        spotLight.shadow.mapSize.height = 2048; 
        spotLight.target.position.set(5, 3, 0);
        spotLight.shadowDarkness = 1;
        spotLight.bias = 0.001;
        DontDestroy.mainScene.add( spotLight );

        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );

        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var tex = new Array();
        var tex_sequence = 0;
        var numOfMaterials = 4;
        var materialCounter = 0;
        var assetPath = RoboVisualizer.AssetsPath + 'assets/models/ice_terrain';
        if(DestroyOnJourney.Terrain == null || DestroyOnJourney.Terrain == undefined)
        {
            loadJSON(assetPath + ".js", function(response){
                var jsonObj = JSON.parse(response.body);
                if(DontDestroy.JSONLoader == null)
                    DontDestroy.JSONLoader = new THREE.JSONLoader();
                var model = DontDestroy.JSONLoader.parse(jsonObj, RoboVisualizer.TexturesPath, function(){
                    if(model.geometry != undefined && model.materials != null)
                    {
                        var material = new THREE.MultiMaterial(model.materials);
                        DestroyOnJourney.Terrain = new THREE.Mesh(model.geometry, material);
                        for(var prop in material.materials) {
                            if(material.materials[prop] != undefined && material.materials[prop].map != null) {
                                material.materials[prop].map.wrapS = THREE.RepeatWrapping;
                                material.materials[prop].map.wrapT = THREE.RepeatWrapping;
                                tex.push(material.materials[prop].map.clone());
                                if(tex[tex_sequence] != undefined)
                                    tex[tex_sequence].needsUpdate = true;
                                tex_sequence++;
                            }
                        }
                        DestroyOnJourney.Terrain.traverse ( function (child) {
                            if (child instanceof THREE.Mesh) {
                                child.castShadow = true;
                                child.receiveShadow = true;
                            }
                        });
                        DestroyOnJourney.Terrain.position.x = 5.54;
                        DestroyOnJourney.Terrain.position.y = 0;
                        DestroyOnJourney.Terrain.position.z = -5.5;
                        DestroyOnJourney.Terrain.scale.x = DestroyOnJourney.Terrain.scale.y = DestroyOnJourney.Terrain.scale.z = 0.957;
                        DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
                        materialCounter++;
                        if(materialCounter == numOfMaterials)
                            callback();
                    }
                    else
                    {
                        var errorType = "LoadingError";
                        var errorMsg = "Error loading \"" + assetPath + "\"";
                        var caller = arguments.callee.name;
                        var err = new VisualizerError(errorType, errorMsg, caller);
                        visualizer.SetError(err);
                        callback(err);
                    }
                });
            });
        }
        else
        {
            DontDestroy.mainScene.add(DestroyOnJourney.Terrain);
            callback();
        }   
    }
}

Scenes["iceworld2d"] = {
    Initialize: function(divID, callback)
    {
        container = document.getElementById(divID);
        if (DontDestroy.mainCamera == undefined)
            DontDestroy.mainCamera = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        if(DontDestroy.mainScene == undefined)
            DontDestroy.mainScene = new THREE.Scene();
        if(DestroyOnJourney.BGScene == undefined)
            DestroyOnJourney.BGScene = new THREE.Scene();
        if(DontDestroy.BGCamera == undefined)
            DontDestroy.BGCamera = new THREE.OrthographicCamera(-Configurations.RendererSize_x/2, Configurations.RendererSize_x/2, Configurations.RendererSize_y/2, -Configurations.RendererSize_y/2, 0, 30);
        if(DontDestroy.TextureLoader == undefined)
            DontDestroy.TextureLoader = new THREE.TextureLoader();

        // setup LCD
        lcd = new LCD();
        //lights
        DestroyOnJourney.ambientLight = new THREE.AmbientLight(0xffffff, 0.85);
        DontDestroy.mainScene.add( DestroyOnJourney.ambientLight );
       
        // DestroyOnJourney.spotLight = new THREE.SpotLight(0xffffff, 0.5, 250, 0.21, 0, 0);
        // DestroyOnJourney.spotLight.position.set(60, 90, -100 );
        // //Set up shadow properties for the light
        // DestroyOnJourney.spotLight.castShadow = true;
        // DestroyOnJourney.spotLight.shadowCameraVisible = true;
        // DestroyOnJourney.spotLight.shadow.mapSize.width = 2048;  
        // DestroyOnJourney.spotLight.shadow.mapSize.height = 2048; 
        // DestroyOnJourney.spotLight.target.position.set(5, 3, 0);
        // DestroyOnJourney.spotLight.shadowDarkness = 1;
        // DestroyOnJourney.spotLight.bias = 0.001;
        // DontDestroy.mainScene.add( DestroyOnJourney.spotLight );
        
        //setup renderer
        SetupRenderer();
        container.appendChild( DontDestroy.renderer.domElement );
        
        this.DrawTerrain(callback);
    },
    DrawTerrain: function(callback)
    {
        var spriteMap = DontDestroy.TextureLoader.load(RoboVisualizer.TexturesPath + "iceworld2d.jpg",
            function(texture)
            {
                texture.needsUpdate = true;
                var spriteMaterial = new THREE.SpriteMaterial({map: texture});
                var sprite = new THREE.Sprite(spriteMaterial);
                sprite.scale.set(Configurations.RendererSize_x, Configurations.RendererSize_y*1.045, 0.1);
                sprite.renderOrder = 1;
                DestroyOnJourney.BGScene.add(sprite);
                RoboVisualizer.gameFlags.showLoadingScene = false;
                callback();
            },
            function()
            {},
            function loadingScene_Initialize()
            {
                var errorType = "LoadingError";
                var errorMsg = "Error loading \"" + RoboVisualizer.TexturesPath + "iceworld2d.jpg" + "\"";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                visualizer.SetError(err);
                callback(err);
            }
        );
    },
    LateInitialize: function()
    {
        // if(DestroyOnJourney.particleSystem == undefined)
        // {
        //     if(DontDestroy.ParticlesCam == undefined)
        //         DontDestroy.ParticlesCam = new THREE.PerspectiveCamera(Configurations.FOV , Configurations.AspectRatio, 0.3, 1000 );
        //     if(DontDestroy.ParticlesScene == undefined)
        //         DontDestroy.ParticlesScene = new THREE.Scene();
        //     Particles.ParticlSystem.push(new SPE.Group({
        //         texture: {
        //             value: THREE.ImageUtils.loadTexture(RoboVisualizer.TexturesPath + 'wind.png')
        //         },
        //         blending: THREE.NormalBlending,
        //         fog: true
        //     }));
        //
        //     emitter = new SPE.Emitter({
        //         particleCount: 100,
        //         maxAge: {
        //             value: 4,
        //         },
        //         position: {
        //             value: new THREE.Vector3( 0, 15, -20 ),
        //             spread: new THREE.Vector3( 60, 15, 40 ),
        //             randomise: true
        //         },
        //         velocity: {
        //             value: new THREE.Vector3( Math.random()*10, -3, 0 )
        //         },
        //         wiggle: {
        //             spread: 30
        //         },
        //         size: {
        //             value: 1,
        //             spread: 1
        //         },
        //         opacity: {
        //             value: [ 1, 1, 1 ]
        //         },
        //         color: {
        //             value: new THREE.Color( 1, 1, 1 ),
        //             spread: new THREE.Color( 0.1, 0.1, 0.1 )
        //         },
        //         angle: {
        //             value: [ 0, 0 ]
        //         }
        //     });
        //
        //     Particles.ParticlSystem[0].addEmitter( emitter );
        //     DontDestroy.ParticlesScene.add( Particles.ParticlSystem[0].mesh );
        //     Particles.clock = new THREE.Clock();
        // }
        // Particles.isReady = true;
    }
}

var SceneManager = {
    SceneName: "",
    Initialize: function(windowFocus, callback)
    {
        xmlInput = null;
        if(RoboVisualizer.gameFlags != undefined)
        {
            if(RoboVisualizer.gameFlags.fieldInitialized)
                Field.clear_env();
        }
        for(i = 0; i < resources.length; i++)
        {
            if(resources[i] != undefined){
                Destroy(resources[i]);
                delete(resources[i]);
            }
            
        }
        resources = [];
        if(DontDestroy.mainScene != undefined)
            Destroy(DontDestroy.mainScene);
        if(DontDestroy.ParticlesScene != undefined)
            Destroy(DontDestroy.ParticlesScene);
        if(DontDestroy.lcdScene != undefined)
            Destroy(DontDestroy.lcdScene);

        clear_objects();
        

        // if(Particles != undefined)
        // {
        //     Particles.isReady = false;
        //     if(Particles.ParticlSystem != undefined)
        //     {
        //         for(i = 0; i < Particles.ParticlSystem.length; i++)
        //             Particles.ParticlSystem[i].dispose()
        //     }
        //     Particles.ParticlSystem = [];
        // }

        THREE.Cache.enabled = true;
        RoboVisualizer.gameFlags = new GameFlags();
        if(RoboVisualizer.gameFlags.windowFocused == undefined)
        {
            if(windowFocusTemp == undefined)
                RoboVisualizer.gameFlags.windowFocused = windowFocus;
            else
                RoboVisualizer.gameFlags.windowFocused = windowFocusTemp;
        }
        GameManager.initialize(function(){
            callback();
        });
    },
    LoadScene: function(divID, sceneName, callback)
    {   
        if(sceneName != SceneManager.SceneName)
        {
            if(DestroyOnJourney.Terrain != undefined)
            {
                Destroy(DestroyOnJourney.Terrain);
                DestroyOnJourney.Terrain = undefined;
            }
            if(DestroyOnJourney.BGScene != undefined)
                Destroy(DestroyOnJourney.BGScene);
            
                
        }
        SceneManager.SceneName = sceneName;
        RoboVisualizer.gameFlags.isRunning = true;
        var parentDiv = document.getElementById(divID);
        if(parentDiv.hasChildNodes())
        {
            var childCanvas = parentDiv.firstChild;
            parentDiv.removeChild(childCanvas);
        }
        if(Scenes[sceneName] != undefined)
        {
            Scenes[sceneName].Initialize(divID, function(error){
                GameManager.cameraUpdate();
                Sounds.SetupSound(function(){
                    callback();
                });
            });
            GameManager.render();
        }
        else
        {
            var errorType = "Scene Missing";
            var errorMsg = "A scene with name " + sceneName + " does not exist.";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            visualizer.SetError(err);
            callback(err);
        }
    }
};

var DreamManager = {
    sceneName:"",
    enteredDream:false,
    requestId:null,
    Initialize: function(){
        DreamManager.ClearScene();
        if(SceneManager.SceneName == "")
            return false;
        DreamManager.sceneName = SceneManager.SceneName;
        DontDestroy.TextureLoader.load(RoboVisualizer.TexturesPath +DreamManager.sceneName+".jpg",function(texture1){
            DontDestroy.TextureLoader.load(RoboVisualizer.TexturesPath +"birth2"+".jpg",function(texture2){
                DontDestroy.dreamModeCamera = new THREE.OrthographicCamera(-Configurations.RendererSize_x/2, Configurations.RendererSize_x/2, Configurations.RendererSize_y/2, -Configurations.RendererSize_y/2, 30, 30);
                DontDestroy.dreamModeScene = new THREE.Scene();
                DreamManager.enteredDream = false;
                DreamManager.uniforms = {
                    time: { type: "f", value: 1.0 },
                    swirl_radius: { type: "f", value: 10.0 },
                    swirl_amount:{ type: "f", value: 0.0 },
                    resolution: { type: "v2", value: new THREE.Vector2() },
                    mainTexture: { type: "t", value:texture1 },
                    dreamTexture: { type: "t",value:texture2},
                    blend: {type: "f",value: 0.0},
                    dominanceFactor: {type: "f",value: 0.0}
                };
                material = new THREE.ShaderMaterial( {
                    uniforms: DreamManager.uniforms,
                    vertexShader: document.getElementById( 'vertexShader' ).textContent,
                    fragmentShader: document.getElementById( 'fragmentShader' ).textContent
                });

                mesh = new THREE.Mesh( new THREE.PlaneGeometry( 2, 2), material );
                DontDestroy.dreamModeScene.add( mesh );
                DestroyOnJourney.ambientLight2 = new THREE.AmbientLight(0xffffff, 1.0);
                DontDestroy.dreamModeScene.add(DestroyOnJourney.ambientLight2);
                //DontDestroy.dreamModeScene.add( DestroyOnJourney.spotLight );


                DontDestroy.renderer.clear();
                DontDestroy.renderer.render( DestroyOnJourney.BGScene, DontDestroy.BGCamera );
                DontDestroy.renderer.clearDepth();
                DontDestroy.renderer.render( DontDestroy.dreamModeScene, DontDestroy.dreamModeCamera );
                DontDestroy.renderer.clearDepth();
                DontDestroy.renderer.render( DontDestroy.mainScene, DontDestroy.mainCamera );
                DontDestroy.renderer.clearDepth();
                DreamManager.Animate();

            },undefined,function(error){
                console.log("loading  dream mode texture error "+error);

            })

        },undefined,function(error){
            console.log("loading  dream mode texture error "+error);

        })

        // DontDestroy.mainScene.visible = true;
        // DestroyOnJourney.BGScene.visible = false;
        //
        // DontDestroy.renderer.render(DontDestroy.mainScene, DontDestroy.mainCamera);
        // DontDestroy.renderer.clearDepth();
        //
        // var enteredDreamModeEvent = new CustomEvent("enteredDreamMode");
        // document.body.dispatchEvent(enteredDreamModeEvent);

    },
    Animate: function(){
        DreamManager.requestId = requestAnimationFrame( DreamManager.Animate );
        DreamManager.Render();
    },
    Render:function(){
        var step = 1.0/20.0;
        var step2= 1.0/2.0;
        var step4 = 1.0/1000.0;
        if(DreamManager.uniforms.swirl_amount.value < 13.0 && !DreamManager.enteredDream)
            DreamManager.uniforms.swirl_amount.value  += step;
        if(DreamManager.uniforms.swirl_radius.value < 65.0 && !DreamManager.enteredDream)
            DreamManager.uniforms.swirl_radius.value += step2;
        else{

            DreamManager.enteredDream = true;
            DreamManager.uniforms.blend = 1.0;
            step = -(1.0/20.0);
            step2= -(1.0/2.0);
            step3 = (1.0/50.0);
            if(DreamManager.uniforms.dominanceFactor.value+step3 > 1.0)
                DreamManager.uniforms.dominanceFactor.value = 1.0;
            else
                DreamManager.uniforms.dominanceFactor.value += step3;
            if(DreamManager.uniforms.swirl_amount.value > 0.0)
                DreamManager.uniforms.swirl_amount.value  += step;
            if(DreamManager.uniforms.swirl_radius.value > 10.0)
                DreamManager.uniforms.swirl_radius.value += step2;

            else {

                DontDestroy.mainScene.visible = true;
                DestroyOnJourney.BGScene.visible = false;

                DontDestroy.renderer.render(DontDestroy.mainScene, DontDestroy.mainCamera);
                DontDestroy.renderer.clearDepth();
                DontDestroy.renderer.render( DontDestroy.dreamModeScene, DontDestroy.dreamModeCamera );
                DontDestroy.renderer.clearDepth();

                cancelAnimationFrame(DreamManager.requestId);
                var enteredDreamModeEvent = new CustomEvent("enteredDreamMode");
                document.body.dispatchEvent(enteredDreamModeEvent);
                //DreamManager.AnimatePosition();
                //});
            }

        }
        if(!DreamManager.enteredDream){
            var step = (1.0 / 5);
            if (DontDestroy.mainScene.position.y - step <= -15.0) {
                DontDestroy.mainScene.position.y = -15.0;
            }
            else
                DontDestroy.mainScene.position.y -= step;
        }
        else{
            var step = (1.0 / 5);
            if (DontDestroy.mainScene.position.y + step >= 0.0) {
                DontDestroy.mainScene.position.y = 0.0;
            }
            else
                DontDestroy.mainScene.position.y += step;
        }

        DontDestroy.renderer.render( DontDestroy.dreamModeScene, DontDestroy.dreamModeCamera );
        DontDestroy.renderer.clearDepth();
        DontDestroy.renderer.render(DontDestroy.mainScene, DontDestroy.mainCamera);
        DontDestroy.renderer.clearDepth();
    },
    ClearScene:function() {
        if(DontDestroy.dreamModeScene != undefined)
            Destroy(DontDestroy.dreamModeScene);
        if(DontDestroy.dreamModeCamera != undefined)
            Destroy(DontDestroy.dreamModeCamera);
        DreamManager.enteredDream = false;
        DreamManager.requestId = null;
        DreamManager.SceneName = "";
    },
    ReturnToNormalMode:function(){
        RoboVisualizer.Stop();
        DreamManager.enteredDream = false;
        DreamManager.AnimateReverse();
    },
    RenderReverse:function(){
        var step = 1.0/20.0;
        var step2= 1.0/2.0;
        var step4 = 1.0/1000.0;
        if(DreamManager.uniforms.swirl_amount.value < 13.0 && !DreamManager.enteredDream)
            DreamManager.uniforms.swirl_amount.value  += step;
        if(DreamManager.uniforms.swirl_radius.value < 65.0 && !DreamManager.enteredDream)
            DreamManager.uniforms.swirl_radius.value += step2;
        else{

            DreamManager.enteredDream = true;
            DreamManager.uniforms.blend = 0.0;
            step = -(1.0/20.0);
            step2= -(1.0/2.0);
            step3 = -(1.0/50.0);
            if(DreamManager.uniforms.dominanceFactor.value+step3 < 0.0)
                DreamManager.uniforms.dominanceFactor.value = 0.0;
            else
                DreamManager.uniforms.dominanceFactor.value += step3;
            if(DreamManager.uniforms.swirl_amount.value > 0.0)
                DreamManager.uniforms.swirl_amount.value  += step;
            if(DreamManager.uniforms.swirl_radius.value > 10.0)
                DreamManager.uniforms.swirl_radius.value += step2;

            else {

                DontDestroy.mainScene.visible = true;
                DestroyOnJourney.BGScene.visible = true;
                DontDestroy.dreamModeScene.visible = false;
                DontDestroy.renderer.render( DestroyOnJourney.BGScene, DontDestroy.BGCamera );
                DontDestroy.renderer.clearDepth();
                DontDestroy.renderer.render(DontDestroy.mainScene, DontDestroy.mainCamera);
                DontDestroy.renderer.clearDepth();
                cancelAnimationFrame(DreamManager.requestId);
                var exitedDreamMode = new CustomEvent("exitedDreamMode");
                document.body.dispatchEvent(exitedDreamMode);
                DreamManager.ClearScene();
                //document.body.dispatchEvent(enteredDreamModeEvent);
                //DreamManager.AnimatePosition();
                //});
                return;
            }

        }
        if(!DreamManager.enteredDream){
            var step = (1.0 / 5);
            if (DontDestroy.mainScene.position.y - step <= -15.0) {
                DontDestroy.mainScene.position.y = -15.0;
            }
            else
                DontDestroy.mainScene.position.y -= step;
        }
        else{
            var step = (1.0 / 5);
            if (DontDestroy.mainScene.position.y + step >= 0.0) {
                DontDestroy.mainScene.position.y = 0.0;
            }
            else
                DontDestroy.mainScene.position.y += step;
        }

        DontDestroy.renderer.render( DontDestroy.dreamModeScene, DontDestroy.dreamModeCamera );
        DontDestroy.renderer.clearDepth();
        DontDestroy.renderer.render(DontDestroy.mainScene, DontDestroy.mainCamera);
        DontDestroy.renderer.clearDepth();
    },
    AnimateReverse:function(){
        DreamManager.requestId = requestAnimationFrame( DreamManager.AnimateReverse );
        DreamManager.RenderReverse();
    }


};


var EnviromentManager = {
    sceneName:"",
    enteredDream:false,
    requestId:null,
    Initialize: function(){
        EnviromentManager.ClearScene();
        if(SceneManager.SceneName == "")
            return false;
        EnviromentManager.sceneName = SceneManager.SceneName;
        DontDestroy.TextureLoader.load(RoboVisualizer.TexturesPath +EnviromentManager.sceneName+".jpg",function(texture1){
            DontDestroy.TextureLoader.load(RoboVisualizer.TexturesPath +"birth"+".png",function(texture2){
                    DontDestroy.dreamModeCamera = new THREE.OrthographicCamera(-Configurations.RendererSize_x/2, Configurations.RendererSize_x/2, Configurations.RendererSize_y/2, -Configurations.RendererSize_y/2, 30, 30);
                    DontDestroy.dreamModeScene = new THREE.Scene();
                    EnviromentManager.enteredDream = false;
                    EnviromentManager.uniforms = {
                        time: { type: "f", value: 1.0 },
                        swirl_radius: { type: "f", value: 10.0 },
                        swirl_amount:{ type: "f", value: 0.0 },
                        resolution: { type: "v2", value: new THREE.Vector2() },
                        mainTexture: { type: "t", value:texture1 },
                        dreamTexture: { type: "t",value:texture2},
                        blend: {type: "f",value: 0.0},
                        dominanceFactor: {type: "f",value: 0.0}
                    };
                    material = new THREE.ShaderMaterial( {
                        uniforms: EnviromentManager.uniforms,
                        vertexShader: document.getElementById( 'vertexShader' ).textContent,
                        fragmentShader: document.getElementById( 'fragmentShader' ).textContent
                    });
                 
                    mesh = new THREE.Mesh( new THREE.PlaneGeometry( 2, 2), material );
                    DontDestroy.dreamModeScene.add( mesh );
                    DestroyOnJourney.ambientLight2 = new THREE.AmbientLight(0xffffff, 1.0);
                    DontDestroy.dreamModeScene.add(DestroyOnJourney.ambientLight2);
                    //DontDestroy.dreamModeScene.add( DestroyOnJourney.spotLight );

                    
                    DontDestroy.renderer.clear();
                    DontDestroy.renderer.render( DestroyOnJourney.BGScene, DontDestroy.BGCamera );
                    DontDestroy.renderer.clearDepth();
                    DontDestroy.renderer.render( DontDestroy.dreamModeScene, DontDestroy.dreamModeCamera );
                    DontDestroy.renderer.clearDepth();
                    DontDestroy.renderer.render( DontDestroy.mainScene, DontDestroy.mainCamera );
                    DontDestroy.renderer.clearDepth();
                    EnviromentManager.Animate();

            },undefined,function(error){
                console.log("loading  enviroment mode texture error "+error);

            })

        },undefined,function(error){
            console.log("loading  enviroment mode texture error "+error);

        })

    },
    Animate: function(){
        EnviromentManager.requestId = requestAnimationFrame( EnviromentManager.Animate );
        EnviromentManager.Render();
    },
    Render:function(){
        var step = 1.0/20.0;
        var step2= 1.0/2.0;
        var step4 = 1.0/1000.0;
        if(EnviromentManager.uniforms.swirl_amount.value < 13.0 && !EnviromentManager.enteredDream)
            EnviromentManager.uniforms.swirl_amount.value  += step;
        if(EnviromentManager.uniforms.swirl_radius.value < 65.0 && !EnviromentManager.enteredDream)
            EnviromentManager.uniforms.swirl_radius.value += step2;
        else{
            
            EnviromentManager.enteredDream = true;
            EnviromentManager.uniforms.blend = 1.0;
            step = -(1.0/20.0);
            step2= -(1.0/2.0);
            step3 = (1.0/50.0);
            if(EnviromentManager.uniforms.dominanceFactor.value+step3 > 1.0)
                EnviromentManager.uniforms.dominanceFactor.value = 1.0;
            else
                EnviromentManager.uniforms.dominanceFactor.value += step3;
            if(EnviromentManager.uniforms.swirl_amount.value > 0.0)
                EnviromentManager.uniforms.swirl_amount.value  += step;
            if(EnviromentManager.uniforms.swirl_radius.value > 10.0)
                EnviromentManager.uniforms.swirl_radius.value += step2;

            else {
                
                DontDestroy.mainScene.visible = true;
                DestroyOnJourney.BGScene.visible = false;
                DontDestroy.renderer.render( DontDestroy.dreamModeScene, DontDestroy.dreamModeCamera );
                DontDestroy.renderer.clearDepth();
                DontDestroy.renderer.render(DontDestroy.mainScene, DontDestroy.mainCamera);
                DontDestroy.renderer.clearDepth();
                cancelAnimationFrame(EnviromentManager.requestId);
                var enteredEnviromentMode = new CustomEvent("enteredEnviromentMode");
                document.body.dispatchEvent(enteredEnviromentMode);
                //EnviromentManager.AnimatePosition();
                //});
            }

        }
        if(!EnviromentManager.enteredDream){
            var step = (1.0 / 5);
            if (DontDestroy.mainScene.position.y - step <= -15.0) {
                DontDestroy.mainScene.position.y = -15.0;
            }
            else
                DontDestroy.mainScene.position.y -= step;
        }
        else{
            var step = (1.0 / 5);
            if (DontDestroy.mainScene.position.y + step >= 0.0) {
                DontDestroy.mainScene.position.y = 0.0;
            }
            else
                DontDestroy.mainScene.position.y += step;
        }
        
        DontDestroy.renderer.render( DontDestroy.dreamModeScene, DontDestroy.dreamModeCamera );
        DontDestroy.renderer.clearDepth();
        DontDestroy.renderer.render(DontDestroy.mainScene, DontDestroy.mainCamera);
        DontDestroy.renderer.clearDepth();
    },
    ClearScene:function() {
        if(DontDestroy.dreamModeScene != undefined)
            Destroy(DontDestroy.dreamModeScene);
        if(DontDestroy.dreamModeCamera != undefined)
            Destroy(DontDestroy.dreamModeCamera);
        EnviromentManager.enteredDream = false;
        EnviromentManager.requestId = null;
        EnviromentManager.SceneName = "";
    },
    ReturnToNormalMode:function(){
        EnviromentManager.enteredDream = false;
        EnviromentManager.AnimateReverse();
    },
    RenderReverse:function(){
        var step = 1.0/20.0;
        var step2= 1.0/2.0;
        var step4 = 1.0/1000.0;
        if(EnviromentManager.uniforms.swirl_amount.value < 13.0 && !EnviromentManager.enteredDream)
            EnviromentManager.uniforms.swirl_amount.value  += step;
        if(EnviromentManager.uniforms.swirl_radius.value < 65.0 && !EnviromentManager.enteredDream)
            EnviromentManager.uniforms.swirl_radius.value += step2;
        else{
            
            EnviromentManager.enteredDream = true;
            EnviromentManager.uniforms.blend = 0.0;
            step = -(1.0/20.0);
            step2= -(1.0/2.0);
            step3 = -(1.0/50.0);
            if(EnviromentManager.uniforms.dominanceFactor.value+step3 < 0.0)
                EnviromentManager.uniforms.dominanceFactor.value = 0.0;
            else
                EnviromentManager.uniforms.dominanceFactor.value += step3;
            if(EnviromentManager.uniforms.swirl_amount.value > 0.0)
                EnviromentManager.uniforms.swirl_amount.value  += step;
            if(EnviromentManager.uniforms.swirl_radius.value > 10.0)
                EnviromentManager.uniforms.swirl_radius.value += step2;

            else {
                
                DontDestroy.mainScene.visible = true;
                DestroyOnJourney.BGScene.visible = true;
                DontDestroy.dreamModeScene.visible = false;
                DontDestroy.renderer.render( DestroyOnJourney.BGScene, DontDestroy.BGCamera );
                DontDestroy.renderer.clearDepth();
                DontDestroy.renderer.render(DontDestroy.mainScene, DontDestroy.mainCamera);
                DontDestroy.renderer.clearDepth();
                cancelAnimationFrame(EnviromentManager.requestId);
                var exitedEnviromentMode = new CustomEvent("exitedEnviromentMode");
                document.body.dispatchEvent(exitedEnviromentMode);
                EnviromentManager.ClearScene();
                //document.body.dispatchEvent(enteredDreamModeEvent);
                //DreamManager.AnimatePosition();
                //});
                return;
            }

        }
        if(!EnviromentManager.enteredDream){
            var step = (1.0 / 5);
            if (DontDestroy.mainScene.position.y - step <= -15.0) {
                DontDestroy.mainScene.position.y = -15.0;
            }
            else
                DontDestroy.mainScene.position.y -= step;
        }
        else{
            var step = (1.0 / 5);
            if (DontDestroy.mainScene.position.y + step >= 0.0) {
                DontDestroy.mainScene.position.y = 0.0;
            }
            else
                DontDestroy.mainScene.position.y += step;
        }
        
        DontDestroy.renderer.render( DontDestroy.dreamModeScene, DontDestroy.dreamModeCamera );
        DontDestroy.renderer.clearDepth();
        DontDestroy.renderer.render(DontDestroy.mainScene, DontDestroy.mainCamera);
        DontDestroy.renderer.clearDepth();
    },
    AnimateReverse:function(){
        EnviromentManager.requestId = requestAnimationFrame( EnviromentManager.AnimateReverse );
        EnviromentManager.RenderReverse();
    }


}