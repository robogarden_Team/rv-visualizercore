//*************************************
//
//RoboGarden Project
//
//*************************************
var pre_objects = [];
var all_objects = [];
var Objects = {
    LoadModel: function(modelPath, callback)
    {
        var stored = resources.find(function(obj){return obj.key == modelPath});
        if(stored != undefined)
        {
           callback(stored.value.geometry, stored.value.material)
        }
        else
        {
            var materialCounter = 0;
            loadJSON(modelPath + ".js", function(response){
                if(response.status == "success")
                {
                    var jsonObj = JSON.parse(response.body);
                    if(DontDestroy.JSONLoader == null)
                        DontDestroy.JSONLoader = new THREE.JSONLoader();
                    var model = DontDestroy.JSONLoader.parse(jsonObj, RoboVisualizer.TexturesPath, function(){
                        if(model.geometry != undefined && model.materials != null)
                        {
                            var material = new THREE.MultiMaterial(model.materials);
                            var item = {};
                            item.key = modelPath;
                            item.value = {geometry: model.geometry, material: material};
                            resources.push(item);
                            materialCounter++;
                            if(materialCounter == model.materials.length)
                                callback(model.geometry, material);
                        }
                        else
                        {
                            var errorType = "LoadingError";
                            var errorMsg = "Error loading \"" + modelPath + "\"";
                            var caller = arguments.callee.name;
                            var err = new VisualizerError(errorType, errorMsg, caller);
                            commandCallback(err);
                        }
                    });
                }
                else
                {
                    var errorType = "LoadingError";
                    var errorMsg = "Error loading \"" + modelPath + "\"";
                    var caller = arguments.callee.name;
                    var err = new VisualizerError(errorType, errorMsg, caller);
                    commandCallback(err);
                }
            });
        }
        
    },
    // CreateObject: function(geo, mat, x, y, name, type,callback)
    // {
    //     var object = new THREE.Mesh(geo, mat);
    //     pre_objects.push({'v_type':type,'v_object':object});
    //     all_objects.push(object);
    //     object.position.z = -y;
    //     object.position.x = x;
    //     object.position.y = 0;
    //     object.name = name;
    //     if(gridObjects != undefined){
    //         if(gridObjects[y] != undefined)
    //             gridObjects[y][x] = object;
    //     }
    //
    //     DontDestroy.mainScene.add(object);
    //     pre_objects.push({'v_type':type,'v_object':object});
    //     all_objects.push(object);
    //     callback();
    // },
    create_Object: function (type,x,y,name,callback) {
        if(type != "none")
        {
            var default_object = null;
            for(var i=0;i<pre_objects.length;i++)
            {
                if(pre_objects[i].v_type == type)
                {
                    var object = pre_objects[i].v_object.clone();
                    all_objects.push(object);
                    object.position.z = -y;
                    object.position.x = x;
                    object.position.y = 0;
                    object.name = name;
                    if(gridObjects != undefined){
                        if(gridObjects[y] != undefined)
                            gridObjects[y][x] = object;
                    }

                    DontDestroy.mainScene.add(object);
                    callback();
                    return;
                }
            }
            var assetPath = RoboVisualizer.AssetsPath + 'assets/models/'+ type;
            Objects.LoadModel(assetPath, function(geo, mat){
                default_object = new THREE.Mesh(geo, mat);
                pre_objects.push({'v_type':type,'v_object':default_object});
                var object = default_object.clone();
                all_objects.push(object);
                object.position.z = -y;
                object.position.x = x;
                object.position.y = 0;
                object.name = name;
                if(gridObjects != undefined){
                    if(gridObjects[y] != undefined)
                        gridObjects[y][x] = object;
                }

                DontDestroy.mainScene.add(object);
                callback();
                //Objects.CreateObject(geo, mat, x, y,name, type, callback);
            });

        }
        else
        {
            callback();
        }
    },
    create_objects_row: function(type, rowNum, pattern)
    {
        if(type != "none")
        {
            var default_object = null;
            for(var i=0;i<pre_objects.length;i++)
            {
                if(pre_objects[i].v_type == type)
                {
                    var row_object = pre_objects[i].v_object.clone();
                    for(var j = 0;j<pattern.length;j++)
                    {
                        if(pattern[j] == 'x')
                        {
                            var object = row_object.clone();
                            all_objects.push(object);
                            object.position.z = -rowNum;
                            object.position.x = j;
                            object.position.y = 0;
                            object.name = "";
                            if(gridObjects != undefined){
                                if(gridObjects[rowNum] != undefined)
                                    gridObjects[rowNum][j] = object;
                            }

                            DontDestroy.mainScene.add(object);
                        }
                    }

                    Destroy(row_object);
                    row_object = undefined;

                    commandCallback();
                    return;
                }
            }
            var assetPath = RoboVisualizer.AssetsPath + 'assets/models/'+ type;
            Objects.LoadModel(assetPath, function(geo, mat){
                default_object = new THREE.Mesh(geo, mat);
                pre_objects.push({'v_type':type,'v_object':default_object});
                for(var j = 0;j<pattern.length;j++)
                {
                    if(pattern[j] == 'x')
                    {
                        var object = default_object.clone();
                        all_objects.push(object);
                        object.position.z = -rowNum;
                        object.position.x = j;
                        object.position.y = 0;
                        object.name = "";
                        if(gridObjects != undefined){
                            if(gridObjects[rowNum] != undefined)
                                gridObjects[rowNum][j] = object;
                        }

                        DontDestroy.mainScene.add(object);
                    }
                }

                commandCallback();
            });


            // else
            // {
            //     var callback;
            //     var assetPath = RoboVisualizer.AssetsPath + 'assets/models/'+ type;
            //     Objects.LoadModel(assetPath, function(geometry, material){
            //         var indx = 0;
            //         callback = function()
            //         {
            //             indx++;
            //             if(indx > pattern.length)
            //                 commandCallback();
            //             else
            //             {
            //                 while(pattern[indx] != 'x')
            //                 {
            //                     if(indx > pattern.length)
            //                     {
            //                         commandCallback();
            //                         return;
            //                     }
            //                     else
            //                         indx++;
            //                 }
            //                 Objects.CreateObject(geometry, material, indx, rowNum, "",type,callback);
            //             }
            //         }
            //         while(pattern[indx] != 'x')
            //         {
            //             if(indx > pattern.length)
            //             {
            //                 commandCallback();
            //                 return;
            //             }
            //             else
            //                 indx++;
            //         }
            //         Objects.CreateObject(geometry, material, indx, rowNum, "",type, callback);
            //     });
            // }

        }
        else
        {
            commandCallback();
        }
    },
    create_objects_col: function(type, colNum, pattern)
    {
        if(type != "none")
        {
            var default_object = null;
            for(var i=0;i<pre_objects.length;i++)
            {
                if(pre_objects[i].v_type == type)
                {
                    var col_object = pre_objects[i].v_object.clone();
                    for(var j = 0;j<pattern.length;j++)
                    {
                        if(pattern[j] == 'x')
                        {
                            var object = col_object.clone();
                            all_objects.push(object);
                            object.position.z = -j;
                            object.position.x = colNum;
                            object.position.y = 0;
                            object.name = "";
                            if(gridObjects != undefined){
                                if(gridObjects[j] != undefined)
                                    gridObjects[j][colNum] = object;
                            }

                            DontDestroy.mainScene.add(object);
                        }
                    }

                    Destroy(col_object);
                    col_object = undefined;

                    commandCallback();
                    return;
                }
            }
            var assetPath = RoboVisualizer.AssetsPath + 'assets/models/'+ type;
            Objects.LoadModel(assetPath, function(geo, mat){
                default_object = new THREE.Mesh(geo, mat);
                pre_objects.push({'v_type':type,'v_object':default_object});
                for(var j = 0;j<pattern.length;j++)
                {
                    if(pattern[j] == 'x')
                    {
                        var object = default_object.clone();
                        all_objects.push(object);
                        object.position.z = -j;
                        object.position.x = colNum;
                        object.position.y = 0;
                        object.name = "";
                        if(gridObjects != undefined){
                            if(gridObjects[j] != undefined)
                                gridObjects[j][colNum] = object;
                        }

                        DontDestroy.mainScene.add(object);
                    }
                }

                commandCallback();
            });


            // else
            // {
            //     var callback;
            //     var assetPath = RoboVisualizer.AssetsPath + 'assets/models/'+ type;
            //     Objects.LoadModel(assetPath, function(geometry, material){
            //         var indx = 0;
            //         callback = function()
            //         {
            //             indx++;
            //             if(indx > pattern.length)
            //                 commandCallback();
            //             else
            //             {
            //                 while(pattern[indx] != 'x')
            //                 {
            //                     if(indx > pattern.length)
            //                     {
            //                         commandCallback();
            //                         return;
            //                     }
            //                     else
            //                         indx++;
            //                 }
            //                 Objects.CreateObject(geometry, material, colNum, indx, "",type, callback);
            //             }
            //         }
            //         while(pattern[indx] != 'x')
            //         {
            //             if(indx > pattern.length)
            //             {
            //                 commandCallback();
            //                 return;
            //             }
            //             else
            //                 indx++;
            //         }
            //         Objects.CreateObject(geometry, material, colNum, indx, "",type, callback);
            //     });
            // }

        }
        else
        {
            commandCallback();
        }
    },
    remove_object: function (type,x, y) {
        if(type != "none")
        {
            if(gridObjects != undefined){
                if(gridObjects[y] != undefined)
                {
                    if(gridObjects[y][x] != undefined && gridObjects[y][x] != null)
                    {
                        //DontDestroy.mainScene.remove(gridObjects[y][x]);
                        Destroy(gridObjects[y][x]);
                        delete(gridObjects[y][x]);



                        // gridObjects[y][x].geometry.dispose();
                        // if(gridObjects[y][x].material.dispose!=undefined)
                        //     gridObjects[y][x].material.dispose();
                        // else
                        // {
                        //     if(gridObjects[y][x].material.materials != undefined)
                        //     {
                        //         for(var a = gridObjects[y][x].material.materials.length-1; a >=0 ; a--)
                        //         {
                        //             if(gridObjects[y][x].material.materials[a].map != null)
                        //                 gridObjects[y][x].material.materials[a].map.dispose();
                        //             gridObjects[y][x].material.materials[a].dispose();
                        //         }
                        //     }
                        // }

                        gridObjects[y][x] = undefined;
                    }
                }
            }
        }
        commandCallback();
    },

    move_object: function(name, x, y)
    {
        if(gridObjects != undefined){
            if(gridObjects[y] != undefined)
            {
                var obj = scene.getObjectByName(name);
                var x0 = obj.position.x;
                var y0 = -obj.position.z;
                if(gridObjects[y0] != undefined)
                    gridObjects[y0][x0] = 0;
                obj.position.x = x;
                obj.position.z = -y;
                gridObjects[y][x] = obj;
            }
        }
        commandCallback();
    }
}