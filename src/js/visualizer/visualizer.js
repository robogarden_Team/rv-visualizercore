var visualizer = {

    mode: "normal",
    dreamSpeedSlowFactor:2000,
    speedFactor:1.0,
    runMode: "env",
    SetFieldSize: function(Width, Height, color, texture)
    {
        Field.field_init(parseInt(Width),parseInt(Height), color, texture);
    },
    SetCellColor: function(x, y, color, texture)
    {
        Field.change_color(parseInt(x), parseInt(y),color, texture, function(){
            commandCallback();
        });
    },
    SetCellColorRow: function(color, texture, row, pattern)
    {
        Field.set_row_color(color, texture, parseInt(row), pattern);
    },
    SetCellColorCol: function(color, texture, col, pattern)
    {
        Field.set_col_color(color, texture, parseInt(col), pattern);
    },
    SetRobot: function(x, y, name, type)
    {
        Robots.create_robot(parseInt(x), parseInt(y), name, type);
    },
    SetCollectable: function(type, x, y, name)
    {
        //check if name is empty string
        Objects.create_Object(type, parseInt(x), parseInt(y), name, function(){
            commandCallback();
        });
    },
    SetObstacle: function(type, x, y, name)
    {
        //check if name is empty string
        Objects.create_Object(type, parseInt(x), parseInt(y), name, function(){
            commandCallback();
        });
    },
    SetObstacleRow: function(type, row, pattern)
    {
        Objects.create_objects_row(type,parseInt(row),pattern);
    },
    SetObstacleCol: function(type, col, pattern)
    {
        Objects.create_objects_col(type,parseInt(col),pattern);
    },
    SetCollectableRow: function(type, row, pattern)
    {
        Objects.create_objects_row(type,parseInt(row),pattern);
    },
    SetCollectableCol: function(type, col, pattern)
    {
        Objects.create_objects_col(type,parseInt(col),pattern);
    },
    ClearEnv: function()
    {
        Field.clear_env(true);
    },
    MoveRobot: function(name, direction, distance, speed)
    {
        Robots.move_robot(name,direction,parseFloat(distance)/20,parseFloat(speed));
    },
    MoveObstacle: function(name, x, y)
    {
        Objects.move_object(name, parseInt(x), parseInt(y));
    },
    RotateRobot: function(name, angle, radius, speed)
    {
        Robots.rotate_robot(name, parseFloat(angle), parseFloat(radius)/20, parseFloat(speed));
    },
    DelayExecution: function(duration)
    {
        if(visualizer.mode == "dream") {
            console.log("acting delay execution " + duration);
            var actDelay = new CustomEvent("actDelay", {
                'detail': {
                    duration: duration
                }
            });
            document.body.dispatchEvent(actDelay);
        }
        setTimeout(function(){
            commandCallback();
        },duration);
    },
    RemoveObject: function(type, x, y)
    {
        Objects.remove_object(type, parseInt(x), parseInt(y));
    },
    PrintLcd: function(message)
    {
        Field.print_msg(message);
    },
    ClearLcd: function()
    {
        Field.clear_lcd(true);
    },
    PrintCell: function(x, y, message, color, fontSize)
    {
        Field.print_on_cell(parseInt(x), parseInt(y), message, color, parseFloat(fontSize));
    },
    PlaySound: function(type)
    {
        Field.play_sound(type);
    },
    StartCelebration: function()
    {
        commandCallback();
    },
    AbortExecution: function()
    {
        Executer.Abort();
    },
    SetError: function(error,cb)
    {
        console.log(error);
        if(cb)
            commandCallback();
    },
    ActRobotColor: function(color){
        if(visualizer.mode == "dream" && visualizer.runMode == "motion") {
            console.log("acting robot color " + color);
            var actRobotColor = new CustomEvent("actRobotColor", {
                'detail': {
                    color: color
                }
            });
            document.body.dispatchEvent(actRobotColor);
            setTimeout(function(){ commandCallback();}, visualizer.dreamSpeedSlowFactor/visualizer.speedFactor);
        }
        else{
            commandCallback();
        }
    },
    ActRobotDistance: function(method,distance){
        if(visualizer.mode == "dream" && visualizer.runMode == "motion") {
            console.log("acting robot distance " + method + " " + distance);
            var actRobotDistance = new CustomEvent("actRobotDistance", {
                'detail': {
                    method: method,
                    distance: distance
                }
            });
            document.body.dispatchEvent(actRobotDistance);
            setTimeout(function(){ commandCallback();}, visualizer.dreamSpeedSlowFactor/visualizer.speedFactor);
        }
        else{
            commandCallback();
        }
    },
    ActRobotText: function(method,text){
        if(visualizer.mode == "dream" && visualizer.runMode == "motion") {
            console.log("acting robot text " + method + " " + text);
            var actRobotText = new CustomEvent("actRobotText", {
                'detail': {
                    method: method,
                    text: text
                }
            });
            document.body.dispatchEvent(actRobotText);
            setTimeout(function(){ commandCallback();}, visualizer.dreamSpeedSlowFactor/visualizer.speedFactor);

        }
        else{
            commandCallback();
        }
    },
    ActDropItem: function(){
        if(visualizer.mode == "dream" && visualizer.runMode == "motion") {
            console.log("acting drop item");
            var actDropItem = new CustomEvent("actDropItem");
            document.body.dispatchEvent(actDropItem);
            setTimeout(function(){ commandCallback();}, visualizer.dreamSpeedSlowFactor/visualizer.speedFactor);
        }
        else{
            commandCallback();
        }
    },
    ActMoveRobot:function(distance,type){
        //act for distance backward
        if(visualizer.mode == "dream" && visualizer.runMode == "motion") {
            console.log("acting move "+type+ " " + distance);
            var actRobotMove = new CustomEvent("actRobotMove", {
                'detail': {
                    distance: distance,
                    type:type
                }
            });
            document.body.dispatchEvent(actRobotMove);
            commandCallback();
            //setTimeout(function(){ commandCallback();}, visualizer.dreamSpeedSlowFactor);
        }
        else{
            commandCallback();
        }

    },

    ActRotateRobot:function(angle,radius,type){
        //act for distance backward
        if(visualizer.mode == "dream" && visualizer.runMode == "motion") {
            console.log("acting rotate "+type+ " " + radius);
            var actRobotRotate = new CustomEvent("actRobotRotate", {
                'detail': {
                    angle: angle,
                    radius:radius,
                    type:type
                }
            });
            document.body.dispatchEvent(actRobotRotate);
            commandCallback();
            //setTimeout(function(){ commandCallback();}, visualizer.dreamSpeedSlowFactor);
        }
        else{
            commandCallback();
        }

    }
};