var windowFocusTemp = undefined;
var GameEvents = {
    Initialize: function()
    {
        document.body.addEventListener("onSceneInitialize", HandleSceneInitializeEvent, false);
        document.body.addEventListener("OnLoadingSceneInitialize", HandleLoadingSceneInitializeEvent, false);
        document.body.addEventListener("onSceneLoad", HandleSceneLoadEvent, false);
        document.body.addEventListener("onGridInit", HandleGridInitEvent, false);
        document.body.addEventListener("onGridInitFinished", HandleGridInitFinishedEvent, false);
        document.body.addEventListener("onPlayBtnClicked", HandlePlayBtnClicked, false);
        document.body.addEventListener("onPlayFinished", HandlePlayFinishedEvent, false);
        document.body.addEventListener("onPauseEvent", HandlePauseEvent, false);
        document.body.addEventListener("onContinueEvent", HandleContinueEvent, false);
        document.body.addEventListener("onStopEvent", HandleStopEvent, false);
        document.body.addEventListener("onWin", HandleWinEvent, false);
        document.body.addEventListener("onLose", HandleLoseEvent, false);
        document.body.addEventListener("onMuteBtnClicked", HandleMuteBtnClicked, false);
        document.body.addEventListener("onPlaySoundsBtnClicked", HandlePlaySoundsBtnClicked, false);
        document.body.addEventListener("onMissionExit", HandleMissionExit, false);
        document.body.addEventListener("reOrientCamera", reOrientCamera, false);
        document.body.addEventListener("hideBGScene",hideBGScene,false);
        document.body.addEventListener("showBGScene",showBGScene,false);
        document.body.addEventListener("enterDreamMode",enterDreamMode,false);
        document.body.addEventListener("exitDreamMode",exitDreamMode,false);
        document.body.addEventListener("enterEnviromentMode",enterEnviromentMode,false);
        document.body.addEventListener("exitEnviromentMode",exitEnviromentMode,false);
        document.body.addEventListener("speedUp",speedup,false);
        document.body.addEventListener("speedDown",speeddown,false);
        
    }
};

function HandleSceneInitializeEvent(e) 
{
    RoboVisualizer.SceneInit(e.detail.assetspath, e.detail.windowFocus, e.detail.callback);
}

function HandleLoadingSceneInitializeEvent(e)
{
    RoboVisualizer.LoadingSceneInit(e.detail.callback);
}

function HandleSceneLoadEvent(e)
{
    if(RoboVisualizer.gameFlags.inMissionPage)
        RoboVisualizer.SceneLoad(e.detail.parentdiv, e.detail.scenename, e.detail.callback);
}

function HandleGridInitEvent(e) 
{
    if(RoboVisualizer.gameFlags.inMissionPage)
        RoboVisualizer.Init(e.detail.motionscript, e.detail.callback);
}
 
function HandleGridInitFinishedEvent(e) 
{
    if(RoboVisualizer.gameFlags.inMissionPage)
    {
        // if(Scenes[SceneManager.SceneName] != undefined)
        // {
        //     if(Scenes[SceneManager.SceneName].LateInitialize != undefined)
        //     {
        //         Scenes[SceneManager.SceneName].LateInitialize();
        //     }
        // }
        loadingScreen.ClearScene();
        RoboVisualizer.gameFlags.playBtnClicked = false;
        RoboVisualizer.gameFlags.showLoadingScene = false;
        RoboVisualizer.gameFlags.isRunning = false;
        Sounds.PlayMainTheme();
        if(sysCallback != null && sysCallback != undefined)
            sysCallback(e.detail.error);
    }
    else
    {
        
        RoboVisualizer.gameFlags.isRunning = false;
    }
}

function reOrientCamera(){
    RoboVisualizer.gameFlags.isRunning = true;
    GameManager.cameraUpdate();
    GameManager.render();
    RoboVisualizer.gameFlags.isRunning = false;
}
 
function HandlePlayBtnClicked(e) 
{
    RoboVisualizer.gameFlags.isStopped = false;
    if(!RoboVisualizer.gameFlags.isRunning)
    {
        RoboVisualizer.gameFlags.isRunning = true;
        GameManager.render();
    }
    RoboVisualizer.CompileTestSuit(e.detail.input, e.detail.callback);
}

function HandlePlayFinishedEvent(e) 
{
    RoboVisualizer.gameFlags.isRunning = false;
    RoboVisualizer.gameFlags.playBtnClicked = false;
}

function HandlePauseEvent(e) 
{
    RoboVisualizer.Pause();
}

function HandleContinueEvent(e) 
{
    RoboVisualizer.Continue();
}

function HandleStopEvent(e) 
{
    RoboVisualizer.Stop();
}

function HandleWinEvent(e)
{
    Sounds.StopMainTheme();
    Sounds.PlayWinSound();
}

function HandleLoseEvent(e)
{
    Sounds.StopMainTheme();
    Sounds.PlayLoseSound();
}

function HandleMuteBtnClicked()
{
    Sounds.Mute();
}

function HandlePlaySoundsBtnClicked()
{
    Sounds.Play();
}

function HandleMissionExit()
{
    if(RoboVisualizer.gameFlags != undefined)
        RoboVisualizer.gameFlags.inMissionPage = false;
    Executer.Abort(false);
    RoboVisualizer.gameFlags.isRunning = false;
    Sounds.StopMainTheme();
    clear_objects();
}


function hideBGScene(){
    if(DestroyOnJourney.BGScene != null && GameManager != null){
        DestroyOnJourney.BGScene.visible = false;
        GameManager.render();
    }
}

function showBGScene(){
    if(DestroyOnJourney.BGScene != null && GameManager != null){
        DestroyOnJourney.BGScene.visible = true;
        GameManager.render();
    }
}

function enterDreamMode(){
    visualizer.mode = "dream";
    visualizer.speedFactor = 1.0;
    DreamManager.Initialize();
}

function exitDreamMode(){
    visualizer.mode = "normal";
    visualizer.speedFactor = 1.0;
    DreamManager.ReturnToNormalMode();

};

function enterEnviromentMode(){
    visualizer.mode = "enviroment";
    visualizer.speedFactor = 1.0;
    EnviromentManager.Initialize();
};

function exitEnviromentMode(){
    visualizer.mode = "normal";
    visualizer.speedFactor = 1.0;
    EnviromentManager.ReturnToNormalMode();
};

function speedup(){
    if(visualizer.speedFactor >= 2)
        visualizer.speedFactor = 2;
    else
        visualizer.speedFactor = visualizer.speedFactor*2;
}


function speeddown(){

    if(visualizer.speedFactor <= 0.5)
        visualizer.speedFactor = 0.5;
    else
        visualizer.speedFactor = visualizer.speedFactor/2;
}
// window.onfocus = function()
// {
//     if(RoboVisualizer.gameFlags == undefined)
//         windowFocusTemp = true;
//     else
//         RoboVisualizer.gameFlags.windowFocused = true;
//     if(RoboVisualizer.gameFlags != undefined)
//     {
//         if(RoboVisualizer.gameFlags.shouldFinsishGridInit)
//         {
//             RoboVisualizer.gameFlags.shouldFinsishGridInit = false;
//             var initFinish = new CustomEvent("onGridInitFinished",{'detail': {
//                 error: undefined
//             }});
//             document.body.dispatchEvent(initFinish);
//         }
//     }      
// }

// window.onblur = function()
// {
//     if(RoboVisualizer.gameFlags == undefined)
//         windowFocusTemp = false;
//     else
//         RoboVisualizer.gameFlags.windowFocused = false;
// }