function VisualizerError(type, msg, caller)
{
    this.type = type;
    this.message = msg;
    this.callerFunction = caller;
}