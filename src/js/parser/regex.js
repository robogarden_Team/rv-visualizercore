var regex =
    {
        comments: /#.*/g,
        multipleSpaces: / +/g,
        multipleLinebreaks: /\n+ ?\n+/g,
        commandKeywords: /-?\d+\.\d+|-?\d+|\w+|"[^"]*"/g,
        integer: /^-?\d+$/,
        float: /^-?\d+\.\d+$|^-?\d+$/,
        word: /^[a-z_]+\w+$/,
        quote: /^"[^"]*"$/,
        pattern: /^"[-*x*]+"$/,
        direction: /^forward$|^backward$/,
    };
