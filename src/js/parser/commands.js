var commandList = {};

// set fieldsize 10 10
// sets the grid cells size 
commandList["set_fieldsize"] = {
    width: 0,
    height: 0,
    color: "white",
    texture: "strok_" + SceneManager.SceneName,
    testParameters: function(params)
    {
        if(regex.integer.test(params[0]) && regex.integer.test(params[1]))
            return true;
        else
            return false;
    },
    execute: function(params){
        if(params.length >= 2)
        {
            if(this.testParameters(params))
            {
                width = params[0];
                height = params[1];
                color = "white";
                texture = "strok_" + SceneManager.SceneName;
                if(params.length > 3)
                {
                    color = params[2];
                    texture = params[3];
                }
                else if(params.length > 2)
                {
                     texture = params[2];
                }
                visualizer.SetFieldSize(width,height, color, texture);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set fieldsize " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set fieldsize " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// set robot 1 1 robot1
// sets the robot location
commandList["set_robot"] = {
    x: 0,
    y: 0,
    name: "robo",
    type: "robot",
    testParameters: function(params)
    {
        if(regex.integer.test(params[0]) && regex.integer.test(params[1]) && regex.word.test(params[2]))
        {
            return true;
        }
        else
        {
            return false;
        }
    },
    execute: function(params){
        if(params.length >= 3)
        {
            if(this.testParameters(params))
            {
                x = params[0];
                y = params[1];
                name = params[2];
                type = "robot";
                if(params.length > 3)
                    type = params[3];
                visualizer.SetRobot(x,y,name,type);
                
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set robot " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set robot " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// set cellcolor x y red
// sets the cell color 
commandList["set_cellcolor"] = {
    x: 0,
    y: 0,
    color: 0xffffff,
    texture: "tile",
    testParameters: function(params)
    {
        if(regex.integer.test(params[0]) && regex.integer.test(params[1]) && regex.word.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length >= 3)
        {
            if(this.testParameters(params))
            {
                x = params[0];
                y = params[1];
                color = "white";
                texture = "tile";
                if(params.length == 3)
                {
                    if(params[2] == "none")
                    {
                        texture = "strok_" + SceneManager.SceneName;
                    }
                    else
                    {
                        color = params[2];
                    }
                }
                    
                else if(params.length == 4){
                    texture = params[3];
                    if(texture == "water" || texture == "thin_ice" || texture == "cracked_ice" || texture == "transparent")
                        color = "white";
                    else
                        color = "grey";
                }
                visualizer.SetCellColor(x, y, color, texture);
                
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set cellcolor " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set cellcolor " + params + ": aramater count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

commandList["set_cellcolorrow"] = {
    color: "white",
    texture: "tile",
    row: 0,
    pattern:"----------",
    testParameters: function(params)
    {
        if(params.length == 3)
        {
            if(regex.word.test(params[0]) && regex.integer.test(params[1]) && regex.pattern.test(params[2]))
                return true;
            else
                return false;
        }
        else if(params.length == 4)
        {
            if(regex.word.test(params[0]) && regex.word.test(params[1]) && regex.integer.test(params[2]) && regex.pattern.test(params[3]))
                return true;
            else
                return false;
        }
    },
    execute: function(params)
    {
        if(params.length >= 3)
        {
            if(this.testParameters(params))
            {
                if(params.length == 3)
                {
                    if(params[0] == "none")
                    {
                        color = "white";
                        texture = "strok_" + SceneManager.SceneName;
                        row = params[1];
                        pattern = params[2];
                    }
                    else
                    {
                        color = params[0];
                        texture = "tile";
                        row = params[1];
                        pattern = params[2];
                    }
                }
                else
                {
                    texture = params[1];
                    if(texture == "water" || texture == "thin_ice" || texture == "cracked_ice" || texture == "transparent")
                        color = "white";
                    else
                        color = "grey";
                    row = params[2];
                    pattern = params[3];
                }
                visualizer.SetCellColorRow(color, texture, row, pattern);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set cellcolorrow " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set cellcolorrow " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

commandList["set_cellcolorcol"] = {
    color: "white",
    texture: "tile",
    col: 0,
    pattern:"----------",
    testParameters: function(params)
    {
       if(params.length == 3)
        {
            if(regex.word.test(params[0]) && regex.integer.test(params[1]) && regex.pattern.test(params[2]))
                return true;
            else
                return false;
        }
        else if(params.length == 4)
        {
            if(regex.word.test(params[0]) && regex.word.test(params[1]) && regex.integer.test(params[2]) && regex.pattern.test(params[3]))
                return true;
            else
                return false;
        }
    },
    execute: function(params)
    {
        if(params.length >= 3)
        {
            if(this.testParameters(params))
            {
                    if(params.length == 3)
                    {
                        if(params[0] == "none")
                        {
                            color = "white";                    
                            texture = "strok_" + SceneManager.SceneName;
                            col = params[1];
                            pattern = params[2];
                        }
                        else
                        {
                            color = params[0];                    
                            texture = "tile";
                            col = params[1];
                            pattern = params[2];
                        }
                    }
                    else
                    {

                        texture = params[1];
                        if(texture == "water" || texture == "thin_ice" || texture == "cracked_ice" || texture == "transparent")
                            color = "white";
                        else
                            color = "grey";
                        col = params[2];
                        pattern = params[3];
                    }
                    visualizer.SetCellColorCol(color, texture, col, pattern);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set cellcolorcol " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set cellcolorcol " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// set collectable coin 1 1 (coin1)
// sets a collectable object in the grid
commandList["set_collectable"] = {
    type: "coin",
    x: 0,
    y: 0,
    name: "",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.integer.test(params[1]) && regex.integer.test(params[2]))
        {
           return true;
        }
        else
        {
            return false;
        }
    },
    execute: function(params)
    {
        if(params.length >= 3)
        {
            if(this.testParameters(params))
            {
                type = params[0];
                x = params[1];
                y = params[2];
                name = "";
                if(params.length > 3)
                    name = params[3];
                visualizer.SetCollectable(type,x,y,name);
                           
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set collectable " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set collectable " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// set obstaclerow water 1 --xxx--x--
// sets obstacles in specific row according to given pattern
commandList["set_collectablerow"] = {
    type: "coin",
    row: 0,
    pattern: "",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.integer.test(params[1]) && regex.pattern.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length >= 3)
        {
            if(this.testParameters(params))
            {
                type = params[0];
                row = params[1];
                pattern = params[2];
                visualizer.SetCollectableRow(type, row, pattern);
                
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set collectablerow " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set collectablerow " + params + ": aramters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// set obstaclecol water 1 --xxx--x--
// sets obstacles in specific coloumn according to given pattern
commandList["set_collectablecol"] = {
    type: "coin",
    col: 0,
    pattern: "",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.integer.test(params[1]) && regex.pattern.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length >= 3)
        {
            if(this.testParameters(params))
            {
                type = params[0];
                col = params[1];
                pattern = params[2];
                visualizer.SetCollectableCol(type, col, pattern);
                
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set collectablecol " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set collectablecol " + params + ": aramters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// set obstacle water 1 1 (water1)
// sets an obstacle in the grid
commandList["set_obstacle"] = {
    type: "water",
    x: 0,
    y: 0,
    name: "",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.integer.test(params[1]) && regex.integer.test(params[2]))
        {
           return true;
        }
        else
        {
            return false;
        }
    },
    execute: function(params)
    {
        if(params.length == 3 || params.length == 4)
        {
            if(this.testParameters(params))
            {
                type = params[0];
                x = params[1];
                y = params[2];
                name = "";
                if(params.length == 4)
                    name = params[3];
                visualizer.SetObstacle(type,x,y,name);
                           
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set obstacle " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set obstacle " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// set obstaclerow water 1 --xxx--x--
// sets obstacles in specific row according to given pattern
commandList["set_obstaclerow"] = {
    type: "water",
    row: 0,
    pattern: "",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.integer.test(params[1]) && regex.pattern.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 3)
        {
            if(this.testParameters(params))
            {
                type = params[0];
                row = params[1];
                pattern = params[2];
                visualizer.SetObstacleRow(type, row, pattern);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set obstaclerow " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set obstaclerow " + params + ": aramters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// set obstaclecol water 1 --xxx--x--
// sets obstacles in specific coloumn according to given pattern
commandList["set_obstaclecol"] = {
    type: "water",
    col: 0,
    pattern: "",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.integer.test(params[1]) && regex.pattern.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 3)
        {
            if(this.testParameters(params))
            {
                type = params[0];
                col = params[1];
                pattern = params[2];
                visualizer.SetObstacleCol(type, col, pattern);   
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "set obstaclecol " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "set obstaclecol " + params + ": aramters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// clear env
// clears the environment
commandList["clear_env"] = {
    execute: function(params)
    {
        visualizer.ClearEnv();
    }
};

// move robot robo forward 20 2.5
// moves the robot forward or backword given distance with given speed
commandList["move_robot"] = {
    name: "robo",
    direction: "forward",
    distance: 0,
    speed: 0,
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.direction.test(params[1]) && regex.float.test(params[2]) && regex.float.test(params[3]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 4)
        {
            if(this.testParameters(params))
            {
                name = params[0];
                direction = params[1];
                distance = params[2];
                speed = params[3];
                visualizer.MoveRobot(name, direction, distance, speed);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "move robot " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "move robot " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// move obstacle water1 1 1
// moves an obstacle to given cell position
commandList["move_obstacle"] = {
    name: "water1",
    x: 0,
    y: 0,
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.integer.test(params[1]) && regex.integer.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 3)
        {
            if(this.testParameters(params))
            {
                name = params[0];
                x = params[1];
                y = params[2];
                visualizer.MoveObstacle(name, x, y);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "move obstacle " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "move obstacle " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// rotate robot 90 2.5 3
// rotates the robot with given angle, speed and radius
commandList["rotate_robot"] = {
    name: "robo",
    angle: 0,
    radius: 0,
    speed: 0,
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.float.test(params[1]) && regex.float.test(params[2]) && regex.float.test(params[3]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 4)
        {
            if(this.testParameters(params))
            {
                name = params[0];
                angle = params[1];
                radius = params[2];
                speed = params[3];
                visualizer.RotateRobot(name, parseFloat(angle), parseFloat(radius), parseFloat(speed));
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "rotate robot " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "rotate robot " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// remove object coin 1 1
// removes object from the grid
commandList["remove_object"] = {
    type: "coin",
    x: 0,
    y: 0,
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.integer.test(params[1]) && regex.integer.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 3 || params.length == 4)
        {
            if(this.testParameters(params))
            {
                type = params[0];
                x = params[1];
                y = params[2];
                visualizer.RemoveObject(type, x, y);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "remove object " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "remove object " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// print robotvalue robo 50
// prints the robot value on the screen
commandList["print_lcd"] = {
    message: "",
    testParameters: function(params)
    {
        if(regex.quote.test(params[0]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 1)
        {
            if(this.testParameters(params))
            {
                message = params[0];
                visualizer.PrintLcd(message);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "print robotvalue " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "print robotvalue " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

commandList["clear_lcd"] = {
    execute: function()
    {
        visualizer.ClearLcd();
    }
};

// print cell 1 1 "this is cell 1,1"
// prints a given message on a specified cell
commandList["print_cell"] = {
    x: 0,
    y: 0,
    message: "",
    color: "black",
    fontSize: 11,
    testParameters: function(params)
    {
        if(regex.integer.test(params[0]) && regex.integer.test(params[1]) && regex.quote.test(params[2]))
        {
            if(params.length == 4)
            {
                if(regex.word.test(params[3]))
                    return true;
                else
                    return false;
            }
            else if(params.length == 5)
            {
                if(regex.word.test(params[3]) && regex.float.test(params[4]))
                    return true;
                else
                    return false;
            }
            else
                return true;
        }
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length >= 3)
        {
            if(this.testParameters(params))
            {
                x = params[0];
                y = params[1];
                message = params[2];
                color = "black";
                fontSize = "11";
                if(params.length == 4)
                    color = params[3];
                else if(params.length == 5)
                {
                    color = params[3];
                    fontSize = params[4];
                }
                visualizer.PrintCell(x, y, message, color, fontSize);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "print cell " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "print cell " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// play sound move
// plays sound of given type
commandList["play_sound"] = {
    type: "move",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 1)
        {
            if(this.testParameters(params))
            {
                type = params[0];
                visualizer.PlaySound(type);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "play sound " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "play sound " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// start celebration
commandList["start_celebration"] = {
    execute: function(params)
    {
        visualizer.StartCelebration();
    }
};

// delay execution 1000
// delays the execution for givin timeby ms
commandList["delay_execution"] = {
    duration: 0,
    testParameters: function(params)
    {
        if(regex.integer.test(params[0]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 1)
        {
            if(this.testParameters(params))
            {
                duration = params[0];
                visualizer.DelayExecution(duration);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "delay execution " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "delay execution " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

// abort execution
// aborts the execution of the given commands
commandList["abort_execution"] = {
    execute: function(params)
    {
        Executer.Abort();
    }
};

// report error
commandList["set_syntaxerror"] = {
    msg: "",
    execute: function(params)
    {
        if(params.length == 1)
        {
            msg = params[0];
            var errorType = "SimulatioEngineError";
            var errorMsg = msg;
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            visualizer.SetError(err, true);
        }
    }
};

// ignore
commandList["collect_object"] = {
    execute: function(params)
    {
        commandCallback();
    }
};

// ignore
commandList["read_robottext"] = {
    execute: function(params)
    {
        commandCallback();
    }
};

// ignore
commandList["get_robotcolor"] = {
    execute: function(params)
    {
        commandCallback();
    }
}

// ignore
commandList["get_robotdistance"] = {
    execute: function(params)
    {
        commandCallback();
    }
};

// ignore
commandList["drop_collectedobject"] = {
    execute: function(params)
    {
        visualizer.ActDropItem();
    }
};

// ignore
commandList["start_collision"] = {
    execute: function(params)
    {
        commandCallback();
    }
};

// ignore
commandList["set_autocolor"] = {
    execute: function(params)
    {
        commandCallback();
    }
};

// ignore
commandList["set_score"] = {
    execute: function(params)
    {
        commandCallback();
    }
};

// ignore
commandList["set_failure"] = {
    execute: function(params)
    {
        commandCallback();
    }
};

// ignore
commandList["set_maxblocks"] = {
    execute: function(params)
    {
        commandCallback();
    }
};



//act commands
commandList["act_get_robotcolor"] = {
    color: "black",
    name: "robo",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.word.test(params[1]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 2)
        {
            if(this.testParameters(params))
            {
                name = params[0];
                color = params[1];
                visualizer.ActRobotColor(color);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "act get_robotcolor " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "act get_robotcolor " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }


    }
};

commandList["act_get_robotdistance"] = {
    distance: 0,
    name: "robo",
    method:"normal",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.word.test(params[1]) && regex.integer.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 3)
        {
            if(this.testParameters(params))
            {
                name = params[0];
                method = params[1];
                distance = params[2];
                visualizer.ActRobotDistance(method,distance);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "act get_robotdistance " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "act get_robotdistance " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }


    }
};
commandList["act_get_robottext"] = {
    text: "",
    name: "robo",
    method:"text",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.word.test(params[1]) && regex.word.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 3)
        {
            if(this.testParameters(params))
            {
                name = params[0];
                method = params[1];
                text = params[2];
                visualizer.ActRobotText(method,text);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "act get_robottext " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "act get_robottext " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }


    }
};


commandList["oriente_robot"] = {
    name: "robo",
    angle: 0,
    radius: 0,
    speed: 0,
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.float.test(params[1]) && regex.float.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 3)
        {
            if(this.testParameters(params))
            {
                name = params[0];
                angle = params[1];
                speed = params[2];
                visualizer.RotateRobot(name, parseFloat(angle), 0.0, parseFloat(speed));
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "oriente robot " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "oriente robot " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }
    }
};

commandList["act_move_robot"] = {
    text: "",
    name: "robo",
    distance: 0,
    type:"default",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.float.test(params[1]) && regex.word.test(params[2]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 3)
        {
            if(this.testParameters(params))
            {
                name = params[0];
                distance = params[1];
                type = params[2];
                visualizer.ActMoveRobot(distance,type);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "act move_robot " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "act move_robot " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }


    }
};
commandList["act_rotate_robot"] = {
    text: "",
    name: "robo",
    angle: 0,
    radius:0,
    type:"default",
    testParameters: function(params)
    {
        if(regex.word.test(params[0]) && regex.float.test(params[1]) && regex.float.test(params[2]) && regex.word.test(params[3]))
            return true;
        else
            return false;
    },
    execute: function(params)
    {
        if(params.length == 4)
        {
            if(this.testParameters(params))
            {
                name = params[0];
                angle = params[1];
                radius = params[2];
                type = params[3];
                visualizer.ActRotateRobot(angle,radius,type);
            }
            else
            {
                var errorType = "CommandError";
                var errorMsg = "act rotate_robot " + params + ": parameters do not match";
                var caller = arguments.callee.name;
                var err = new VisualizerError(errorType, errorMsg, caller);
                commandCallback(err);
            }
        }
        else
        {
            var errorType = "CommandError";
            var errorMsg = "act rotate_robot " + params + ": parameters count does not match";
            var caller = arguments.callee.name;
            var err = new VisualizerError(errorType, errorMsg, caller);
            commandCallback(err);
        }


    }
};