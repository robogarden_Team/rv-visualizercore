var commandsQueue = [];
var counter = 0;
var scriptCallback;

commandCallback = function(error)
{
    var shouldAbort = false;
    if(error != undefined)
    {
        visualizer.SetError(error);
        if(error.type == "LoadingError")
        {
            shouldAbort = true;
            var initFinish = new CustomEvent("onGridInitFinished",{'detail': {
                error: error
            }});
            document.body.dispatchEvent(initFinish);
        }
        else
        {
            errorsQueue.push(error);
        }
    }
    if(!shouldAbort)
    {
        if(commandsQueue.length > 0)
            Executer.executeNextCommand();
        else
        {
            if(scriptCallback != undefined)
                scriptCallback();
        }
    }
    else
    {
        Executer.Abort(false);
    }
}

var Executer = {
    Init: function(callback)
    {
        counter = 0;
        scriptCallback = callback;
    },
    push: function(command)
    {
        commandsQueue.push(command);
    },
    executeNextCommand: function()
    {
        counter ++;
        var toBeExecuted = commandsQueue.shift();
        if(toBeExecuted.command == null)
        {   
            commandCallback("syntax error: this is not a command");
        }
        else
        {
            console.log('executing '+toBeExecuted.command.constructor.name+' with params '+ toBeExecuted.params);
            toBeExecuted.command.execute(toBeExecuted.params);
        }
    },
    Abort: function(cb)
    {
        counter = 0;
        commandsQueue = [];
        if(cb || cb == undefined)
            commandCallback();
    },
}