
var Parser = 
{
    // split the script into lines where each line represent a single command
    ParseScript: function(script,callback)
    {
        Executer.Init(callback);
        script = Parser.CleanScript(script);
        var commands = script.split('\n');
        var count  = 0;
        for(i=0; i<commands.length; i++)
        {
        	commandString = commands[i];
            commandString = Parser.CleanCommand(commandString);
            Parser.ParseCommand(commandString,count);
            count++;
        }
        Executer.executeNextCommand();
    },

    CleanScript: function(script)
    {
        script = script.replace(regex.comments,"");
        script = script.replace(regex.multipleSpaces, " ");
        script = script.replace(/(\r\n|\r)/gm,"\n");
        script = script.replace(regex.multipleLinebreaks, "\n");
        script = script.trim();
        return script;
    },

    // this function clean the command from the special characters and unwanted spaces
    CleanCommand: function(commandString)
    {
        commandString = commandString.trim();
        return commandString;
    },

    // parsing the command
    ParseCommand: function(commandString, commandID)
    {
        var keywords = commandString.match(regex.commandKeywords);
        if(keywords != null)
        {
            if(keywords.length >= 2)
            {
                var key = keywords[0].toLowerCase() + "_" + keywords[1].toLowerCase();
                var _command = commandList[key];
                if(keywords.length > 2)
                    Executer.push({command:_command, params: keywords.slice(2,keywords.length)});
                else
                    Executer.push({command: _command, params: []});
            }
            else
            {
                Executer.push({command: null, params: []});
            }
        }
    }
}