var renderer;
var scene;
var camera;
var cameraHUD;
var sceneHUD;
var lcdClock;
var lcdTimeout = 0;
var loadingScene;
var loadingSceneCam;
var loadingSceneClock;
var loadingSceneClockTimeout;
var DontDestroy = {};
var DestroyOnJourney = {};
var resources = [];

var GameManager = {
    initialize: function(callback)
    {
        loadJSON(RoboVisualizer.AssetsPath + "VisualizerConfig.json", function(response){
            var configs = JSON.parse(response.body);
            Configurations.RendererSize_x = configs.RendererSize_x;
            Configurations.RendererSize_y = configs.RendererSize_y;
            Configurations.AspectRatio = configs.AspectRatio;
            Configurations.FOV = configs.FOV;
            Configurations.cameraPos_x = configs.cameraPos_x;
            Configurations.cameraPos_y = configs.cameraPos_y;
            Configurations.cameraPos_z = configs.cameraPos_z;
            Configurations.cameraLookAt_x = configs.cameraLookAt_x;
            Configurations.cameraLookAt_y = configs.cameraLookAt_y;
            Configurations.cameraLookAt_z = configs.cameraLookAt_z;
            Configurations.lcdSize_x = configs.lcdSize_x;
            Configurations.lcdSize_y = configs.lcdSize_y;
            Configurations.lcdPos_y = configs.lcdPos_y;
            Configurations.orthoCamZoom = configs.orthoCamZoom;
            callback();
        });
    },
    cameraUpdate: function()
    {
        if(!RoboVisualizer.gameFlags.isRunning)
            return;
        requestAnimationFrame( GameManager.cameraUpdate );
        DontDestroy.mainCamera.aspect = Configurations.AspectRatio;
        DontDestroy.mainCamera.updateProjectionMatrix();
        var pos = new THREE.Vector3 (Configurations.cameraLookAt_x , Configurations.cameraLookAt_y, Configurations.cameraLookAt_z);
        DontDestroy.mainCamera.lookAt( pos );
        DontDestroy.mainCamera.position.set(Configurations.cameraPos_x, Configurations.cameraPos_y, Configurations.cameraPos_z);
        // if(Particles.isReady)
        // {
        //     if(RoboVisualizer.gameFlags != undefined && RoboVisualizer.gameFlags.particlesOn)
        //     {
        //         DontDestroy.ParticlesCam.aspect = Configurations.AspectRatio;
        //         DontDestroy.ParticlesCam.updateProjectionMatrix();
        //         DontDestroy.ParticlesCam.lookAt( pos );
        //         DontDestroy.ParticlesCam.position.set(Configurations.cameraPos_x, Configurations.cameraPos_y, Configurations.cameraPos_z);
        //     }
        // }
        DontDestroy.renderer.setSize(Configurations.RendererSize_x, Configurations.RendererSize_y);
    },
    render: function()
    {
        if(!RoboVisualizer.gameFlags.showLoadingScene)
        {
            if(DestroyOnJourney.BGScene != undefined)
            {
                DontDestroy.renderer.render( DestroyOnJourney.BGScene, DontDestroy.BGCamera );
                DontDestroy.renderer.clearDepth();
            }
            if(DontDestroy.dreamModeScene != undefined)
            {
                DontDestroy.renderer.render( DontDestroy.dreamModeScene, DontDestroy.dreamModeCamera );
                DontDestroy.renderer.clearDepth();
            }
            DontDestroy.renderer.render( DontDestroy.mainScene, DontDestroy.mainCamera );
            // if(Particles.isReady)
            // {
            //     if(RoboVisualizer.gameFlags != undefined && RoboVisualizer.gameFlags.particlesOn)
            //     {
            //         var dt = Particles.clock.getDelta();
            //         for(i = 0; i < Particles.ParticlSystem.length; i++)
            //         {
            //             var random_boolean = Math.random() >= 0.5;
            //             if(Particles.clock.running && Particles.clock.getElapsedTime () > Particles.timeout && random_boolean)
            //             {
            //                 var pos = Particles.positions[Math.floor(Math.random()*4) + 4*i];
            //                 Particles.ParticlSystem[i].triggerPoolEmitter( 2, pos );
            //                 if(i == Particles.ParticlSystem.length-1)
            //                     Particles.timeout += Math.random() * 4;
            //             }
            //             Particles.ParticlSystem[i].tick( dt );
            //         }
            //         DontDestroy.renderer.render( DontDestroy.ParticlesScene, DontDestroy.ParticlesCam );
            //     }
            // }
        }
        if(RoboVisualizer.gameFlags.isDisplaying)
        {
             DontDestroy.renderer.render( DontDestroy.lcdScene, DontDestroy.lcdCamera);
        }
        if(!RoboVisualizer.gameFlags.isRunning)
            return;
        //fps
        var thisFrameUpdate = new Date;
        fps = 1000/(thisFrameUpdate - lastFrameUpdate);
        lastFrameUpdate = thisFrameUpdate;
        var id = requestAnimationFrame( GameManager.render );
        if(lcdClock.running && lcdClock.getElapsedTime () > lcdTimeout)
        {
            RoboVisualizer.gameFlags.isDisplaying = false;
            lcdTimeout = 0;
            lcdClock.stop();
        }
        if(RoboVisualizer.gameFlags.showLoadingScene)
        {
            if(loadingSceneClock.running && loadingSceneClock.getElapsedTime() > loadingSceneClockTimeout)
            {
                loadingSceneClockTimeout+=2;
                loadingScreen.Blink();
                setTimeout(function() {
                    loadingScreen.Blink();
                }, 250);
            }
                
            DontDestroy.renderer.clearDepth();
            DontDestroy.loadingSceneCam.updateProjectionMatrix();
            DontDestroy.renderer.render(DontDestroy.loadingScene,DontDestroy.loadingSceneCam);
        }
        TWEEN.update();
    }
};