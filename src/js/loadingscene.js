var loadingScreen = {
    BG: null,
    circle: null,
    robochar: null,
    robo_closed_eyes:null,
    loadintTxt: null,
    Initialize: function(callback)
    {
        // Initialize
        RoboVisualizer.gameFlags.showLoadingScene = true;
        if(DontDestroy.loadingScene == undefined)
            DontDestroy.loadingScene = new THREE.Scene();
        if(DontDestroy.loadingSceneCam == undefined)
        {
            DontDestroy.loadingSceneCam = new THREE.OrthographicCamera(-Configurations.RendererSize_x/2, Configurations.RendererSize_x/2, Configurations.RendererSize_y/2, -Configurations.RendererSize_y/2, 0, 30 );
            DontDestroy.loadingSceneCam.position.set(0, 0, 0);
            DontDestroy.loadingSceneCam.zoom = Configurations.orthoCamZoom;
        }
        loadingSceneClock = new THREE.Clock();

        // BG
        if(loadingScreen.BG == null)
        {
            var material  = new THREE.MeshBasicMaterial({color: 0x617d90, transparent: true});
            material.needsUpdate = true;
            var geometry = new THREE.PlaneGeometry(Configurations.RendererSize_x, Configurations.RendererSize_y);
            loadingScreen.BG = new THREE.Mesh(geometry, material);
            DontDestroy.loadingScene.add( loadingScreen.BG );
        }

        var assetPath0 = RoboVisualizer.AssetsPath + "assets/textures/BG";
        if(loadingScreen.circle == null)
        {
            if(DontDestroy.TextureLoader == undefined)
                DontDestroy.TextureLoader = new THREE.TextureLoader();   
            var spriteMap = DontDestroy.TextureLoader.load( assetPath0 + ".png",
                function(texture)
                {
                    texture.needsUpdate = true;
                    var spriteMaterial = new THREE.SpriteMaterial( {map: texture, transparent: true} );
                    spriteMaterial.opacity = 0.8;
                    var sprite = new THREE.Sprite( spriteMaterial );
                    sprite.renderOrder = 4;
                    var width = spriteMaterial.map.image.width*Configurations.RendererSize_x/1366;
                    var height = spriteMaterial.map.image.height*Configurations.RendererSize_y/585;
                    sprite.scale.set(width, height, 0.1);
                    sprite.position.x = -600;
                    sprite.position.y = -200;
                    loadingScreen.circle = sprite;
                    DontDestroy.loadingScene.add( loadingScreen.circle );
                    loadingScreen.LateInitialize(callback);
                },
                function()
                {
                    // on progress
                },
                function loadingScreen_Initialize()
                {
                    var errorType = "LoadingError";
                    var errorMsg = "Error loading \"" + assetPath0 + "\"";
                    var caller = arguments.callee.name;
                    var err = new VisualizerError(errorType, errorMsg, caller);
                    callback(err);
                }
            );
        }
        else
        {
            loadingScreen.circle.position.x = -600;
            loadingScreen.circle.position.y = -200;
            loadingScreen.LateInitialize(callback);
        }
    },
    LateInitialize: function(callback)
    {
        var assetPath = RoboVisualizer.AssetsPath + "assets/textures/open_eyes";
        if(loadingScreen.robochar == null)
        {
            if(DontDestroy.TextureLoader == undefined)
                DontDestroy.TextureLoader = new THREE.TextureLoader();   
            var spriteMap = DontDestroy.TextureLoader.load( assetPath + ".png",
                function(texture)
                {
                    texture.needsUpdate = true;
                    var spriteMaterial = new THREE.SpriteMaterial( {map: texture} );
                    var sprite = new THREE.Sprite( spriteMaterial );
                    var yPos = Configurations.RendererSize_y/10;
                    sprite.position.set(8, yPos, 0);
                    var width = spriteMaterial.map.image.width*Configurations.RendererSize_x/1366;
                    var height = spriteMaterial.map.image.height*Configurations.RendererSize_y/585;
                    sprite.scale.set(width, height, 0.1);
                    sprite.renderOrder = 3;
                    loadingScreen.robochar = sprite;
                    DontDestroy.loadingScene.add( loadingScreen.robochar );
                },
                function()
                {
                    // on progress
                },
                function loadingScreen_Initialize()
                {
                    var errorType = "LoadingError";
                    var errorMsg = "Error loading \"" + assetPath + "\"";
                    var caller = arguments.callee.name;
                    var err = new VisualizerError(errorType, errorMsg, caller);
                    callback(err);
                }
            );
        }
        else
        {
            loadingScreen.robochar.visible = true;
        }
        
        var assetPath1 = RoboVisualizer.AssetsPath + "assets/textures/loading";
        if(loadingScreen.loadintTxt == null)
        {
            if(DontDestroy.TextureLoader == undefined)
                DontDestroy.TextureLoader = new THREE.TextureLoader();   
            var spriteMap = DontDestroy.TextureLoader.load( assetPath1 + ".png",
                function(texture)
                {
                    texture.needsUpdate = true;
                    var spriteMaterial = new THREE.SpriteMaterial( {map: texture} );
                    var sprite = new THREE.Sprite( spriteMaterial );
                    var yPos = -Configurations.RendererSize_y/6;
                    sprite.position.set(0, yPos, 0);
                    var width = spriteMaterial.map.image.width*Configurations.RendererSize_x/1366;
                    var height = spriteMaterial.map.image.height*Configurations.RendererSize_y/585;
                    sprite.scale.set(width, height, 0.1);
                    sprite.renderOrder = 2;
                    loadingScreen.loadintTxt = sprite;
                    DontDestroy.loadingScene.add( loadingScreen.loadintTxt );  
                },
                function()
                {
                    // on progress
                },
                function loadingScreen_Initialize()
                {
                    var errorType = "LoadingError";
                    var errorMsg = "Error loading \"" + assetPath1 + "\"";
                    var caller = arguments.callee.name;
                    var err = new VisualizerError(errorType, errorMsg, caller);
                    callback(err);
                }
            );
        }
        else
        {
            loadingScreen.loadintTxt.material.opacity = 1;
        }

        var assetPath2 = RoboVisualizer.AssetsPath + "assets/textures/closed_eyes";
        if(loadingScreen.robo_closed_eyes == null)
        {
            if(DontDestroy.TextureLoader == undefined)
                DontDestroy.TextureLoader = new THREE.TextureLoader();   
            var spriteMap = DontDestroy.TextureLoader.load( assetPath2 + ".png",
                function(texture)
                {
                    texture.needsUpdate = true;
                    var spriteMaterial = new THREE.SpriteMaterial( {map: texture} );
                    var sprite = new THREE.Sprite( spriteMaterial );
                    var yPos = Configurations.RendererSize_y/10;
                    sprite.position.set(8, yPos, 0);
                    var width = spriteMaterial.map.image.width*Configurations.RendererSize_x/1366;
                    var height = spriteMaterial.map.image.height*Configurations.RendererSize_y/585;
                    sprite.scale.set(width, height, 0.1);
                    sprite.renderOrder = 2;
                    loadingScreen.robo_closed_eyes = sprite;
                    loadingScreen.robo_closed_eyes.visible = false;
                    DontDestroy.loadingScene.add( loadingScreen.robo_closed_eyes );
                    loadingScreen.AnimateCircle(); 
                    callback();
                },
                function()
                {
                    // on progress
                },
                function loadingScreen_Initialize()
                {
                    var errorType = "LoadingError";
                    var errorMsg = "Error loading \"" + assetPath2 + "\"";
                    var caller = arguments.callee.name;
                    var err = new VisualizerError(errorType, errorMsg, caller);
                    callback(err);
                }
            );
        }
        else
        {
            loadingScreen.robo_closed_eyes.visible = false;
            loadingScreen.AnimateCircle();
            callback();
        }

    },
    ClearScene: function()
    {
        //Destroy(loadingScene);
        TWEEN.removeAll();
    },
    StartProgress: function()
    {
        loadingSceneClockTimeout = 0;
        loadingSceneClock.start();
        var loadingOpacity = {a: loadingScreen.loadintTxt.material.opacity};
        var targetOpacity = {a: 1 - loadingOpacity.a};
        var decreaseOpacity = new TWEEN.Tween(loadingOpacity)
            .to({a: 0}, 500)
            .delay(0)
            .easing(TWEEN.Easing.Linear.None)
            .onUpdate(function(){
                loadingScreen.loadintTxt.material.needsUpdate = true;
                loadingScreen.loadintTxt.material.opacity = loadingOpacity.a;
            });

        var increaseOpacity = new TWEEN.Tween(loadingOpacity)
            .to({a: 1}, 500)
            .delay(0)
            .easing(TWEEN.Easing.Linear.None)
            .onUpdate(function(){
                loadingScreen.loadintTxt.material.needsUpdate = true;
                loadingScreen.loadintTxt.material.opacity = loadingOpacity.a;
            });

        decreaseOpacity.chain(increaseOpacity);
        increaseOpacity.chain(decreaseOpacity);
        decreaseOpacity.start();    
    },
    Blink: function()
    {
        loadingScreen.robo_closed_eyes.visible = true;
        loadingScreen.robochar.visible = false;
        setTimeout(function() {
            loadingScreen.robochar.visible = true;
            loadingScreen.robo_closed_eyes.visible = false;
        }, 100);
    },
    AnimateCircle: function()
    {
        var currentPos = {x: loadingScreen.circle.position.x, y: loadingScreen.circle.position.y, z: loadingScreen.circle.position.z};
        var targetPos1 = {x: loadingScreen.circle.position.x, y: loadingScreen.circle.position.y+400, z: loadingScreen.circle.position.z};
        var targetPos2 = {x: loadingScreen.circle.position.x+900, y: loadingScreen.circle.position.y+400, z: loadingScreen.circle.position.z};
        var targetPos3 = {x: 0, y: 0, z: 0};

        var circleTween1 = new TWEEN.Tween(currentPos)
        .to(targetPos1, 1000)
        .delay(0)
        .easing(TWEEN.Easing.Linear.None)
        .onUpdate(function(){
            loadingScreen.circle.position.x = currentPos.x;
            loadingScreen.circle.position.y = currentPos.y;
            loadingScreen.circle.position.z = currentPos.z;
        })
        .onComplete(function(){
        });
        var circleTween2 = new TWEEN.Tween(currentPos)
        .to(targetPos2, 2000)
        .delay(10)
        .easing(TWEEN.Easing.Linear.None)
        .onUpdate(function(){
            loadingScreen.circle.position.x = currentPos.x;
            loadingScreen.circle.position.y = currentPos.y;
            loadingScreen.circle.position.z = currentPos.z; 
        })
        .onComplete(function(){
        });
        var circleTween3 = new TWEEN.Tween(currentPos)
        .to(targetPos3, 300)
        .delay(10)
        .easing(TWEEN.Easing.Linear.None)
        .onUpdate(function(){
            loadingScreen.circle.position.x = currentPos.x;
            loadingScreen.circle.position.y = currentPos.y;
            loadingScreen.circle.position.z = currentPos.z;
        })
        .onComplete(function(){
            loadingScreen.StartProgress();
        });

        circleTween1.chain(circleTween2);
        circleTween2.chain(circleTween3);
        circleTween1.start();
    }
};