README 1.26.0
* Bug Fixes:
    - Fix for Camera Adjust by timeout for running flag


README 1.20.0

* New Features:
    - Sound Effects [main theme - win - lose]
    - Particles

* Updates:
    - Remove tab focus workaround

* Bug Fixes:
    - On focus bug fix
    - Sound bug fix
    - stop rendering on mission exit
    - abort mission executing on mission exit
    - keep rendering after motion script finishes
    - destroy particles on mission start


README 1.19.0

* New Features:
    - image environment


README 1.18.0

* Bug Fixes:
    - loading scene BG color is lighter in winter bug fix.
    - changing tabs bug fix.
    - terrain caching problem

README 1.17.0

* Bug Fixes:
    - merge with light hotfix


README 1.16.0

* Bug Fixes:
    - stop then play then continue bug fix.


README 1.15.0

* Bug Fixes:
    - tab switch bug fix


README 1.14.0

* New Features:
    - new volcanic island scene with assets.
    - new moon scene with assets.
    - new golden mountain scene with assets.
    - new ice world scene with assets.

* Updates:
    - combined object textures for birthday - moon - golden mountain - volcanic island assets, TexturesPath added.
    - combined terrain textures for birthday - moon - golden mountain - volcanic island terrains.

* Bug Fixes:
    - object placement is 0 on the Y access.
    - loading scene minor enhancement.
    - golden scene adjustment.
    - handle scene not found error.
    - set cellcolor none handled
    - tab focus bug fix
    - minor bug fixes
    - play btn bug fix


README 1.13.0

* Updates:
    - loading sequence changes.
    - grass object removed from the scene.

* Bug Fixes:
    - undo stop on failure.
    - load object bug fix by material counter.
    - handle error reporting.
    - minor bug fixes.
    - fix "print_on_cell" old scene object bug.

README 1.11.0

* Updates:
    - stop and reset the visualizer on failure.

* Bug Fixes:
    - remove grass model.


README 1.10.0

* New Features:
    - new birthday scene.

* Updates:
    - cell texture changes with scene name.


README 1.9.0

* Bug Fixes:
    - loading scene disappears before terrain materials are set.
    - removed unneeded functions from robot.js


README 1.8.0

* Updates: 
    - compressed scene texture added

* Bug Fixes:
    - robot disappeares on run bug
    - model terrain merge problem


README 1.7.0

* New Features:
    - WebglSupported api added

* Updates: 
    - configuration file added to gitignore
    - destroy resources array
    - new winter scene updates
    - image environment
    - scene components containers (DontDestroy, DestroyOnJourney, resources)

* Bug Fixes:
    - Memory consumption enhancements
    - minor bug fixes


README 1.6.0

* Updates:
    - game configrations is being read form json file
    - remove manual cache and enable browser cache
    - remove birthday scene from scene manager
    - return to manual cache and free the memory on load
    - return to browser cache
    - destroy scenes onload
    - dont destroy the renderer
    - ClearScene api added

* Bug Fixes:
    - structure enhancements (added GameManager anc lcd classes)
    - LCD is not being removed bug fix
    - memory leak bu fixed
    - clear env bug fix


README 1.5.0

* New Features:
    - loading scene animation

* Updates:
    - start rendering the scene after the loading scene is done

* Bug Fixes:
    - files order in gulpfile
    - game events intialize function
    - grid initialized callback dont turn loading screen off


README 1.4.0

* New Features
    - added event handlers

* Updates
    - loading screen changed to splash screen
    - updated textureLoader
    - loaing screen BG is dark grey
    - game flags organized in one class
    - loading text color is white

* Bug Fixes
    - adjustments in winter scene lights and shadows
    - set cellcolor(col/row) callback is fired after the commands are finished
    - game flags bug fix
    - tester call back timeout


README 1.3.0

* Updates:
    - camera position added to configrations file
    - sys-callback is waiting for a single frame before pausing the rendering loop
    - loading scene time decreased to 5 seconds

* Bug Fixes
    - json loader onload callback modified inside three.js lib
    - the 'water texture is not yet loaded in the scene before pausing the rendering loop' bug fixed


README 1.2.0

* New Features:
    - Error Handling

* Updates:
    - increase loading time to 10 sec
    - loading scene configurations dynamically
    
* Bug Fixes
    - use loading manager with json loader to make sure textures are mapped to the model before callback
    - adjust renderer size
    - reserving callbacks bug


README 1.1.0

* New Features:
    - added loading screen when loading the visualizer

* Updates:
    - loading scene with robochar logo

* Bug Fixes
    - wait until scene is completely loaded before running env-script
    - stop, continue and restart bug fixed
    - reinitializing the scene bug fixed
    - memory management enhancements
    - loading scene dont close unless scene is ready


README 1.0.0

* New Features:
    - implementing the visualizer usnig Three.js
    - winter scene and birthday scene
    - winter assets
    - new robot

* Updates:
    - enhancements in env-script and motion-script commands
    - journies: birthday1, birthday2 and winter updated to new commands and scene design